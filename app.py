"""
Flask app file to be run to render web app
"""

# Import modules
from flask import Flask, flash, redirect, url_for, render_template, request, Response, send_file, make_response, abort, session
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
import pandas as pd
import matplotlib.pyplot as plt

import glob
import os
import re

import collections
import numpy as np
import pandas as pd
import string
import csv
import json
import datetime
import math
import pickle
import operator
import calendar
import copy

from functions import *



# Configure Flask app and SQL database
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shifts.db'

app.secret_key = "zjd92kn"

# Profile pics
UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

SQLALCHEMY_TRACK_MODIFICATIONS = False
db = SQLAlchemy(app)		# database

class Permanent_Shifts(db.Model):
	"""Permanent Shifts"""
	id = db.Column(db.Integer, primary_key=True)
	ShiftName = db.Column(db.String(80), nullable=False)
	StartTime = db.Column(db.Integer)
	EndTime = db.Column(db.Integer)

	def __repr__(self):
		return f"DoctorShifts('{self.ShiftName}', '{self.StartTime}', '{self.EndTime}')"

#list of all doctors in Emergency Department
list_of_doctors_total_csv = pd.read_csv('list_of_doctors.csv', sep=';', header=None)
list_of_doctors_total_csv = list_of_doctors_total_csv[0].tolist()

list_of_last_names = []
for doctor in list_of_doctors_total_csv:
	list_of_last_names.append(doctor.split(',')[0].strip().lower())

with open('shift_types.json') as w:
	shift_types = json.load(w)


# global variables used to display 
patient_inflow = 0
patients_seen = 0
avg_waittime = 0

metric = ""
schedule_tables_dict = {}
background_color = []

analysis_table = ""
analysis_pkg = []

individual_pph_list = {}
individual_pi_list = {}
individual_pia_list = {}

pph_sum_list = []
pi_sum_list = []
pia_sum_list = []

df_schedule = pd.DataFrame()

BBavailable_doctors = []
BBavailable_shifts = []

pph_minus_pi_difference_list = []					# list of difference between PPH and PI

global_search_date = datetime.datetime.now()
global_search_date_end = datetime.datetime.now()

#Generalized Time, Weekday and Month
time_index = ['7:00 am', '8:00 am', '9:00 am', '10:00 am', '11:00 am', '12:00 pm',
		'1:00 pm', '2:00 pm', '3:00 pm', '4:00 pm', '5:00 pm', '6:00 pm',
		'7:00 pm', '8:00 pm', '9:00 pm', '10:00 pm', '11:00 pm', '12:00 am',
		'1:00 am', '2:00 am', '3:00 am', '4:00 am', '5:00 am', '6:00 am']
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
complete_weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


#Machine Learning Models
pickle_in_one = open('models/hour_PPH_regression.pickle','rb')		
pickle_in_two = open('models/hour_PI_regression.pickle', 'rb')		
pickle_in_three = open('models/day_PPH_regression.pickle', 'rb')
pickle_in_four = open('models/day_PI_regression.pickle', 'rb')
pickle_in_five = open('models/hour_PIA_regression.pickle', 'rb')
pickle_in_six = open('models/day_PIA_regression.pickle', 'rb')

hourModelPPH = pickle.load(pickle_in_one)
hourModelPI = pickle.load(pickle_in_two)
dayModelPPH = pickle.load(pickle_in_three)
dayModelPI = pickle.load(pickle_in_four)
hourModelPIA = pickle.load(pickle_in_five)
dayModelPIA = pickle.load(pickle_in_six)

# global parameter to render login animation template
render_login_animation = True


#LOGIN PAGE
@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
	error = None
	global username
	global render_login_animation
	global logged_in_global_variable

	if request.method == 'POST':
		username = (request.form['doctor_name']).split(',')[0].strip().lower()
		password = request.form['password']
		if (username == 'admin' and password == 'admin') or (username=="tester" and password=="tester"):
			session['username'] = request.form['doctor_name']
			logged_in_global_variable = True
			return redirect(url_for('home'))
		else:
			error = 'Invalid credentials. Please try again!'

	if render_login_animation:
		render_login_animation = False
		return render_template('login.html', error=error)
	else:
		return render_template('login_without_animation.html', error=error)

@app.route("/logout")
def logout():
	"""Function to log out"""

	global logged_in_global_variable

	session.pop("username", None)
	logged_in_global_variable = False

	return redirect(url_for("login"))



#ADMINISTRATOR

#Home Page
@app.route('/home', methods=['GET', 'POST'])
def home():
	global logged_in_global_variable

	# check login status
	if "username" in session:

		# if submit request
		if request.method == "POST":
			global schedule_tables_dict
			global df_schedule
			global metric

			global individual_pph_list
			global individual_pi_list
			global individual_pia_list

			global pph_sum_list
			global pi_sum_list
			global pia_sum_list

			global global_search_date
			global global_search_date_end

			pph_sum_list.clear()
			pi_sum_list.clear()
			pia_sum_list.clear()

			doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
			schedule_tables_dict.clear()

			global BBavailable_doctors
			global BBavailable_shifts
			BBavailable_doctors.clear()
			BBavailable_shifts.clear()

			key = 'Emergence'

			# error handling with file
			try:
				f = request.files["file"]

				# search dates
				search_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
				search_date_end = datetime.datetime.strptime(request.form["search_date_end"],"%Y-%m-%d")
				global_search_date = search_date
				global_search_date_end = search_date_end

				metric = request.form['metrics']


			except:
				return render_template("error_message.html", error_message="Please make sure input is correctly formatted")


			df_schedule = pd.read_excel(f, header=6)
			df_schedule.set_index('Dates', inplace=True)

			#add_shifts_to_database(db, Shifts, df_schedule, list_of_doctors_total_csv)
			calendar_dict, all_calendar_days, html_calendars = get_calendars(search_date, search_date_end, months)

			global patients_seen
			global patient_inflow
			global avg_waittime

			global backup_shifts 
			global weekends 
			global night_shifts
			global day_shifts
			global total_hours

			global pph_minus_pi_difference_list
			pph_minus_pi_difference_list.clear()

			BBavailable_doctors, BBavailable_shifts = get_BB_doctors_and_shifts(df_schedule, list_of_last_names, list_of_doctors_total_csv, BBavailable_doctors, BBavailable_shifts, search_date, all_calendar_days)

			print('Creating Restriction Trackers...')									#Restrictions Trackers
			backup_shifts = {name: 0 for name in BBavailable_doctors}					#Track number of backup shifts
			weekends = {name: 0 for name in BBavailable_doctors}						#Track number of weekends
			night_shifts = {name: 0 for name in BBavailable_doctors}					#Track number of night shifts
			day_shifts = {name: 0 for name in BBavailable_doctors}						#Track number of day shifts
			total_hours = {name: 0 for name in BBavailable_doctors} 					#Check total hours worked by physician

			#Background colors and DateIDs
			global background_color
			all_ids = [x for x in range(0, len(all_calendar_days))]

			for day, date in enumerate(all_calendar_days):
				current_date = search_date + datetime.timedelta(day)
				search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")
				temp_row = df_schedule.ix[search_date_string]
				schedule_col_names = df_schedule.columns

				month = search_date_string.split('-')[1]
				month_vec = monthto(months, month)

				day_schedule = pd.DataFrame(index=range(24))
				day_schedule, doctors, shifts = fill_BB_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
				day_schedule_compute = day_schedule.copy()

				for backup_shift in ['Admin on call', 'Backup day', 'Backup night']:
					if backup_shift in day_schedule.columns:
						day_schedule_compute.drop(columns=backup_shift, inplace=True)

				for doctor, shift in zip(doctors, shifts): update_BB_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, total_hours, doctor_shifts, shift_types)

				# vectorize row
				pph_list, pi_list, pia_list, pph_sum, pi_sum, pia_sum = vectorize_row(date, day_schedule_compute, month_vec, hourModelPPH, hourModelPI, hourModelPIA, dayModelPPH, dayModelPI, dayModelPIA, list_of_doctors_total_csv)

				individual_pph_list[day] = pph_list
				individual_pi_list[day] = pi_list
				individual_pia_list[day] = pia_list

				pph_sum_list.append(round(pph_sum))
				pi_sum_list.append(round(pi_sum))
				pia_sum_list.append(round(pia_sum))

				pph_minus_pi_difference_list.append(round(pph_sum) - round(pi_sum))
				#Get colours Overall Score
				Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
				if metric == 'PIA': day_bg_color = calculate_bg_color_with_PIA(pia_sum)
				elif metric == 'PPH-PI': day_bg_color = calculate_bg_color_with_PIP(pph_sum, pi_sum)

				background_color.append(day_bg_color)

				#Design metric for overcrowding if PPH < PI multiple hours in a row

				day_schedule.insert(loc=0, column='Time', value=time_index)
				day_schedule['PPH score'] = pph_list
				day_schedule['Overall score'] = Overall
				day_schedule.replace(np.nan, '', regex = True, inplace=True)
				schedule_tables_dict[day] = day_schedule

			#Configuring calendar
			originals, replacements = configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays)
			html_calendars = configure_html_calendars(originals, replacements, html_calendars)
			carousel_html_calendars = add_carousel_to_html_calendar_table(html_calendars)

			patients_seen = str(int(round(sum(pph_sum_list))))
			patient_inflow = str(int(round(sum(pi_sum_list))))
			avg_waittime = str(int(round(sum(pia_sum_list) / len(pia_sum_list))))

			'''
			x_axis = [x for x in range(len(pph_sum_list))]
			plt.title('Patients seen and patient inflow per day ByteBloc Schedule')
			plt.plot(x_axis, pph_sum_list, label="PPH")
			plt.plot(x_axis, pi_sum_list, label="PI")
			plt.legend()
			plt.savefig('BB_PPH_vs_PI.png', dpi=500)
			plt.close()
			'''

			return render_template('schedule.html', calendar=carousel_html_calendars, patient_inflow=patient_inflow, patients_seen=patients_seen, avg_waittime=avg_waittime, key=key, metric=metric)
		return render_template('home.html')

	return redirect(url_for("login"))



#DISPLAYING CALENDARS AND ANALYSIS PAGES

@app.route('/calendar_graphs', methods=['GET'])
def calendar_graph():

	global logged_in_global_variable

	if "username" in session:

		global global_search_date
		global global_search_date_end
		global pph_sum_list
		global pi_sum_list
		global pia_sum_list

		delta = global_search_date_end - global_search_date
		labels_data = [global_search_date + datetime.timedelta(days=x) for x in range(0, delta.days)]
		
		return render_template('calendar_graphs.html', pph_list=pph_sum_list, pi_list=pi_sum_list, pia_list=pia_sum_list, labels=labels_data)

	return redirect(url_for("login"))


@app.route('/analysis_graphs', methods=['GET'])
def analysis_graph():

	global logged_in_global_variable

	# check session login
	if "username" in session:


		global analysis_pkg
		current_date = analysis_pkg[0]
		pph_list = analysis_pkg[1]
		pi_list = analysis_pkg[2]
		pia_list = analysis_pkg[3]
		labels_data = [(i + 7) % 24 for i in range(24)]
		return render_template('analysis_graphs.html', current_date=current_date, labels=labels_data, pph_list=pph_list, pi_list=pi_list, pia_list=pia_list)

	return redirect(url_for("login"))


@app.route('/analysis', methods=['GET'])
def analysis():
	"""Displays schedule for a specifc day"""

	global logged_in_global_variable

	# check session login status

	if "username" in session:
		global analysis_table
		global analysis_pkg
		return render_template('analysis.html', current_date=analysis_pkg[0], html_table=analysis_table, 
			shift_changes_plus_minus=analysis_pkg[4], pph_sum=analysis_pkg[5], pi_sum=analysis_pkg[6], pia_sum=analysis_pkg[7])

	return redirect(url_for("login"))


@app.route("/update_schedule", methods=["GET", "POST"])
def update_schedule_doctor():
	"""Function to change doctors"""

	global logged_in_global_variable

	# check login status
	if "username" in session:

		global analysis_pkg
		global global_search_date
		global shift_types
		global df_schedule
		global list_of_doctors_total_csv
		global list_of_last_names

		current_date = global_search_date + datetime.timedelta(int(analysis_pkg[-1]))
		search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")

		# doctors for that particular day
		doctor_nested_list = list(zip(list(df_schedule.columns.values), df_schedule.ix[search_date_string]))

		return render_template("update_schedule_doctor.html", change_date=search_date_string, doctor_nested_list=doctor_nested_list, list_of_doctors_total_csv=list_of_last_names)
	return redirect(url_for("login"))


@app.route("/update_schedule_change_df_schedule", methods=['GET', 'POST'])
def update_schedule_df():
	#check login status
	global logged_in_global_variable


	if "username" in session: 
		if request.method == 'POST':
			global background_color
			global metric
			global analysis_pkg
			global global_search_date
			global global_search_date_end
			global shift_types
			global schedule_tables_dict
			global individual_pph_list
			global individual_pi_list
			global individual_pia_list
			global pph_sum_list
			global pi_sum_list
			global pia_sum_list
			global pph_minus_pi_difference_list

			doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
			current_date = global_search_date + datetime.timedelta(int(analysis_pkg[-1]))
			search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")
			calendar_dict, all_calendar_days, html_calendars = get_calendars(global_search_date, global_search_date_end, months)
			all_ids = [x for x in range(0, len(all_calendar_days))]
			key = 'Emergence'
			day = int(analysis_pkg[-1])
			date = current_date.strftime("%Y-%m-%d")
			month = search_date_string.split('-')[1]
			month_vec = monthto(months, month)

			for shift_name in list(shift_types.keys()):
				new_doctor = request.form.get(shift_name)
				df_schedule.ix[search_date_string][shift_name] = new_doctor

			schedule_col_names = df_schedule.columns
			temp_row = df_schedule.ix[search_date_string]

			day_schedule = pd.DataFrame(index=range(24))
			day_schedule, doctors, shifts = fill_BB_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
			day_schedule_compute = day_schedule.copy()

			for backup_shift in ['Admin on call', 'Backup day', 'Backup night']:
				if backup_shift in day_schedule.columns:
					day_schedule_compute.drop(columns=backup_shift, inplace=True)

			#for doctor, shift in zip(doctors, shifts): update_BB_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, total_hours, doctor_shifts, shift_types)

			# vectorize row
			pph_list, pi_list, pia_list, pph_sum, pi_sum, pia_sum = vectorize_row(date, day_schedule_compute, month_vec, hourModelPPH, hourModelPI, hourModelPIA, dayModelPPH, dayModelPI, dayModelPIA, list_of_doctors_total_csv)

			individual_pph_list[day] = pph_list
			individual_pi_list[day] = pi_list
			individual_pia_list[day] = pia_list

			pph_sum_list[day] = round(pph_sum)
			pi_sum_list[day] = round(pi_sum)
			pia_sum_list[day] = round(pia_sum)

			pph_minus_pi_difference_list[day] = round(pph_sum) - round(pi_sum)
			#Get colours Overall Score
			Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
			if metric == 'PIA': day_bg_color = calculate_bg_color_with_PIA(pia_sum)
			elif metric == 'PPH-PI': day_bg_color = calculate_bg_color_with_PIP(pph_sum, pi_sum)

			background_color[day] = day_bg_color	# set background color for that day

			#Design metric for overcrowding if PPH < PI multiple hours in a row

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)
			schedule_tables_dict[day] = day_schedule

			#Configuring calendar
			originals, replacements = configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays)
			html_calendars = configure_html_calendars(originals, replacements, html_calendars)
			carousel_html_calendars = add_carousel_to_html_calendar_table(html_calendars)

			patients_seen = str(int(round(sum(pph_sum_list))))
			patient_inflow = str(int(round(sum(pi_sum_list))))
			avg_waittime = str(int(round(sum(pia_sum_list) / len(pia_sum_list))))

			return render_template('schedule.html', calendar=carousel_html_calendars, patient_inflow=patient_inflow, patients_seen=patients_seen, avg_waittime=avg_waittime, key=key, metric=metric)
		return "error"
	return redirect(url_for("login"))


#Schedule
@app.route('/schedule', methods=['GET', 'POST'])
def schedule():
	"""Render calendar view of schedule"""

	global logged_in_global_variable

	# check login status

	if "username" in session:

		if request.method == "POST":
			global schedule_tables_dict
			global analysis_table
			global analysis_pkg

			global individual_pph_list
			global individual_pi_list
			global individual_pia_list

			global pph_sum_list
			global pi_sum_list
			global pia_sum_list

			global metric
			global pph_minus_pi_difference_list

			date = request.form['date']
			dateID = request.form['dateID']
			getmonth = request.form['month']

			pph_list = individual_pph_list[int(dateID)]
			pi_list = individual_pi_list[int(dateID)]
			pia_list = individual_pia_list[int(dateID)]

			pph_sum = pph_sum_list[int(dateID)]
			pi_sum = pi_sum_list[int(dateID)]
			pia_sum = pia_sum_list[int(dateID)]

			month = getmonth[:3]
			month_vector = monthto(months, month)
			current_date = str(getmonth.split(' ')[0].strip()) + ' ' + str(date) + ', ' + str(getmonth.split(' ')[1].strip())
			wdate = pd.to_datetime(str(getmonth.split(' ')[1].strip()) + '-' + str(months.index(month) + 1) + '-' + str(date), format="%Y-%m-%d")
			weekday = wdate.weekday()
			weekday_vec = [0] * 7
			weekday_vec[weekday] = 1
			raw_table = copy.deepcopy(schedule_tables_dict[int(dateID)])
			raw_table = feature_importance(list_of_doctors_total_csv, raw_table, month_vector, weekday_vec, hourModelPPH)
			html_table = schedule_df_to_html(raw_table)

			shift_plus_minus = calculate_doctor_shifts_plus_minus_per_day(pph_minus_pi_difference_list[int(dateID)])

			analysis_table = html_table
			analysis_pkg = [current_date, pph_list, pi_list, pia_list, shift_plus_minus, pph_sum, pi_sum, pia_sum, dateID]

			return render_template('analysis.html', current_date=current_date, html_table=html_table, 
				shift_changes_plus_minus=shift_plus_minus, pph_sum=pph_sum, pi_sum=pi_sum, pia_sum=pia_sum)
			

		global patient_inflow
		global patients_seen
		global avg_waittime
		global metric
		
		return render_template('schedule.html', patient_inflow=patient_inflow, patients_seen=patients_seen, avg_waittime=avg_waittime, metric=metric)

	return redirect(url_for("login"))


@app.route('/optimized_summary', methods=['GET'])
def optimized_summary():

	global logged_in_global_variable

	# check login status
	if "username" in session:

		"""Display optimized summary"""
		global backup_shifts
		global weekends
		global day_shifts
		global night_shifts
		global total_hours

		summary_table = pd.DataFrame(index=(list(backup_shifts.keys()).sort()))
		summary_table['# Backup Shifts'] = pd.Series(backup_shifts)
		summary_table['# Day Shifts'] = pd.Series(day_shifts)
		summary_table['# Night Shifts'] = pd.Series(night_shifts)
		summary_table['# Weekend Shifts'] = pd.Series(weekends)
		summary_table['# Hours Worked'] = pd.Series(total_hours)

		total = [sum(backup_shifts.values()), sum(day_shifts.values()), sum(night_shifts.values()), sum(weekends.values()), sum(total_hours.values())]
		summary_table.loc['Total'] = total

		html_summary_table = summary_table.to_html(index=True)
		original_string = """<table border="1" class="dataframe">"""
		new_string = """<table style="width: 100%; table-layout:fixed;" class="table table-bordered table-sm table-hover">"""
		html_summary_table = html_summary_table.replace(original_string, new_string)

		for lastname, fullname in zip(list_of_last_names, list_of_doctors_total_csv):
			html_summary_table = html_summary_table.replace(f'<th>{lastname}</th>', f'<th>{fullname}</th>')

		return render_template('optimized_summary.html', summary_table=html_summary_table)

	return redirect(url_for("login"))


#Configure Shifts
@app.route('/shift_configuration', methods=['GET', 'POST'])
def configure_shifts():
	"""Modify shift names and durations"""

	global logged_in_global_variable

	# check login status
	if "username" in session:

		if request.method == 'POST':
			for item in Permanent_Shifts.query.all():
				newShiftName = request.form[item.ShiftName]
				newStartTime = int((request.form[item.ShiftName + ' Start Time']).split(':')[0])
				newEndTime = int((request.form[item.ShiftName + ' End Time']).split(':')[0])
				if newStartTime < 7: newStartTime += 24
				if newEndTime <= 7: newEndTime += 24
				item.ShiftName = newShiftName
				item.StartTime = newStartTime
				item.EndTime = newEndTime

			db.session.commit()

		permanent_shifts = {item.ShiftName: [str(item.StartTime % 24) , str(item.EndTime % 24)] for item in Permanent_Shifts.query.all()}
		for shiftname, time in permanent_shifts.items():
			if len(time[0]) == 1: time[0] = '0' + time[0]
			if len(time[1]) == 1: time[1] = '0' + time[1]		

		return render_template('shift_configuration.html', doctor_shifts=permanent_shifts)

	return redirect(url_for("login"))


#EXTRA FEATURES

# Calculator display
@app.route('/calculator', methods=["GET", "POST"])
def calculator():
	"""Calculator to compute PPH values for a combination of doctors"""

	global logged_in_global_variable

	# check login status
	if "username" in session:

		if request.method == "POST":
			try:
				# get doctor names
				list_of_doctors_current = []
				form_names = ["doctor_1", "doctor_2", "doctor_3", "doctor_4", "doctor_5", "doctor_6"]
				for form_name in form_names:
					list_of_doctors_current.append(request.form[form_name])

				# get time format
				time_military_format = request.form["hour_of_day"]
				time_military_format = int(time_military_format.split(':')[0])

				current_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
				search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")

				weekday_vec = [0] * 7
				current_weekday = current_date.weekday()
				weekday_vec[current_weekday] = 1

				month = search_date_string.split('-')[1]
				month_vector = monthto(months, month)

				# predict
				prediction = backend_predict(list_of_doctors_current, time_military_format, month_vector, weekday_vec, hourModelPPH, list_of_doctors_total_csv)

				time_military_format = 0

				prediction_string = f"<p>The predicted PPH value is <b>{prediction[0]}</b>.</p>"
				prediction_string += "<p>The doctors working were:</p><ul>"

				for doctor_name in list_of_doctors_current:
					if doctor_name != '':
						prediction_string += f"<li>{doctor_name}</li>"
				prediction_string += "</ul>"
				prediction_string += f"""The time was <b>{request.form["hour_of_day"]}</b>.<br>"""
				prediction_string += f"The date was <b>{search_date_string}</b>."

				list_of_doctors_current = []

			# if there is an error
			except ValueError:
				prediction_string = "<h6>Sorry, we're unable to render your submission! You may have left some forms unfilled.</h6>"

			return render_template("calculator.html", list_of_doctors=list_of_doctors_total_csv, prediction=prediction_string)

		# render calculator.html without output if there is no POST request
		return render_template("calculator.html",  list_of_doctors=list_of_doctors_total_csv)
	return redirect(url_for("login"))


@app.route("/display", methods=["GET", "POST"])
def display():
	"""Display results from calculator"""

	global logged_in_global_variable

	# check login status
	if "username" in session:

		# get doctor names
		list_of_doctors_current = []
		form_names = ["doctor_1", "doctor_2", "doctor_3", "doctor_4", "doctor_5"]
		for form_name in form_names:
			list_of_doctors_current.append(request.form[form_name])

		# get time format
		time_military_format = request.form["hour_of_day"]
		time_military_format = int(time_military_format.split(':')[0])

		current_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
		search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")

		weekday_vec = [0] * 7
		current_weekday = current_date.weekday()
		weekday_vec[current_weekday] = 1

		month = search_date_string.split('-')[1]
		month_vector = monthto(months, month)

		# predict
		prediction = backend_predict(list_of_doctors_current, time_military_format, month_vector, weekday_vec, hourModelPPH, list_of_doctors_total_csv)

		list_of_doctors_current = []
		time_military_format = 0

		return render_template("display.html", prediction=prediction[0])

	return redirect(url_for("login"))


@app.route("/download", methods=["GET", "POST"])
def download():
	"""Downloads CSV file for proposed schedule"""

	global logged_in_global_variable

	# check login status
	if "username" in session:

		# list of PPH-PI for each day based on index
		global plus_minus_list
		csv_string = ""		# output string saved to the CSV file

		current_date = global_search_date
		start_date = current_date.strftime('%m/%d/%Y')

		# for day_index: pandas table of schedule in dictionary of proposed schedules
		for index, raw_table in schedule_tables_dict.items():
			csv_string += "\n"
			csv_string += current_date.strftime('%m/%d/%Y')
			csv_string += ", patient difference: " + str(plus_minus_list[index])
			csv_string += "\n"
			header_string = "Shift, Doctor\n"
			csv_string += header_string

			# remove columns with no doctor names
			modified_table = raw_table.drop(columns=["Time",  'PPH score', 'Overall score'])
			current_shifts = modified_table.columns.values

			for shift_name in current_shifts:
				doctor_name = np.unique(modified_table[shift_name].tolist())
				doctor_name = [x for x in doctor_name if x]
				shift_string = '\"'+ shift_name + '\",\"' + doctor_name[0] + '\" \n'
				csv_string += shift_string

			csv_string += '\n'

			end_date = current_date.strftime('%m/%d/%Y')
			current_date = current_date + datetime.timedelta(days=1)

		csv_file_header = f"Recommended Schedule from {start_date} to {end_date}\nMore patients seen: {sum(plus_minus_list)}\n\n"
		csv_string = csv_file_header + csv_string

		# downloads the CSV file
		resp = make_response(csv_string)
		resp.headers["Content-Disposition"] = "attachment; filename=export.csv"
		resp.headers["Content-Type"] = "text/csv"
		return resp
	return redirect(url_for("login"))


if __name__ == '__main__':
	logged_in_global_variable = False
	app.run()
