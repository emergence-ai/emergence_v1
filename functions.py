"""
Ensemble of functions used by main.py
"""

# Import modules
import pandas as pd 
import glob
import os

import random
import collections
import numpy as np
import pandas as pd
import string
import csv
import json
import datetime
import math
import pickle
import operator
import calendar
import itertools

def daterange(date1, date2):
	"""Yield datetime objects"""
	from datetime import timedelta, date
	for n in range(int ((date2 - date1).days)+1):
		yield date1 + timedelta(n)

def allowed_file(filename, ALLOWED_EXTENSIONS):
	"""Check whether image is in valid format
	Return"""
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def process_weights(vector, model, doctor_list):
	"""
	Takes into input the vector for the row and the list of doctor full names

	Returns dictionary of { doctor_name : weight }
	The higher the weight, the more important the doctor is to making the team more efficient
	"""

	number_of_doctors = len(doctor_list)

	doctor_vector = vector[:number_of_doctors]  # vector of doctors
	date_vector = vector[number_of_doctors:]    # vector of hour and date
	overall_number_of_doctors_in_selection = len([i for i, x in enumerate(doctor_vector) if x == 1])

	# overall PPH score
	overall_score = model.predict(np.array([vector]))[0]

	# dictionary with the doctor name key associated to the weight value
	doctor_weights = {}

	# get doctor indices
	doctor_indices = [i for i, x in enumerate(doctor_vector) if x == 1]

	for doctor_index in doctor_indices:

		# get doctor's full name
		doctor_full_name = doctor_list[doctor_index]

		# remove the specified doctor from the list (1 becomes 0)
		temp_doctor_vector = doctor_vector.copy()
		temp_doctor_vector[doctor_index] = 0
		combined_vector = temp_doctor_vector + date_vector

		# compute weight
		prediction = model.predict(np.array([combined_vector]))[0]
		number_of_doctors_in_selection = len([i for i, x in enumerate(temp_doctor_vector) if x == 1])
		weight = overall_score/overall_number_of_doctors_in_selection - prediction/number_of_doctors_in_selection

		# assign key and value to dictionary
		doctor_weights[doctor_full_name] = weight

	return doctor_weights

def vectorize_doctors(list_of_doctors_current, list_of_doctors_total):
	"""
	Return one-hot vector of combination of doctors working at a specific time
	
	list_of_doctors_current is list of doctors currently working
	list_of_doctors_total is list of total doctors
	"""

	doctor_vector = [0] * len(list_of_doctors_total)
	for doctor_name_current in list_of_doctors_current:
		for doctor_name_total in list_of_doctors_total:
			if doctor_name_current == doctor_name_total:
				doctor_vector[list_of_doctors_total.index(doctor_name_current)] = 1

	return doctor_vector

def get_BB_doctors_and_shifts(df_schedule, list_of_last_names, list_of_doctors_total_csv, BBavailable_doctors, BBavailable_shifts, search_date, all_calendar_days):
	"""
	Return list of available doctors and available shifts
	"""

	shift_names = df_schedule.columns	# names of shifts

	# for day, day in all calendar days:
	for day, date in enumerate(all_calendar_days):

		# create valid search string
		search_date_string = (search_date + datetime.timedelta(day)).strftime("%a, %d-%b-%Y").replace(" 0", " ")
		
		# find row corresponding to date search string
		temp_row = df_schedule.ix[search_date_string]
		available_shifts = []
		available_doctors = []

		# for shift_index, doctor in the row
		for shift, doc in enumerate(temp_row):

			# for index, doctor last name in the list of last names:
			for ind, lastname in enumerate(list_of_last_names):

				# if the names are the same
				if doc.lower().strip() == lastname.lower().strip():

					available_shifts.append(shift_names[shift]) 				# add shift to available shifts
					available_doctors.append(list_of_doctors_total_csv[ind])	# add doctor to available doctors
		BBavailable_shifts.append(available_shifts)
		BBavailable_doctors.extend(available_doctors)
	BBavailable_doctors = list(set(BBavailable_doctors))		# unique list of available doctors
	return BBavailable_doctors, BBavailable_shifts

def fill_BB_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule):
	"""
	Return day schedule, doctors working and used shifts

	day_schedule: empty Pandas dataframe schedule (24 rows)
	"""
	doctors = []
	shifts = []

	# for column name and row :
	for column, value in zip(schedule_col_names, temp_row):

		# for each doctor last name
		for ind, lastname in enumerate(list_of_last_names):

			# if they correspond
			if value.lower().strip() == lastname.lower().strip():

				# link range based on doctor shifts
				list_range_hours = doctor_shifts[column]
				shifts.append(column)

				# substract shift hours by 7
				start = list_range_hours[0] - 7
				end = list_range_hours[1] - 7

				# get doctor full name based on index position of last name
				doctor_full_name_current = list_of_doctors_total_csv[ind]
				doctors.append(doctor_full_name_current)
				for hour in range(start, end):
					day_schedule = day_schedule.set_value(hour, column, doctor_full_name_current)   

	return day_schedule, doctors, shifts


def timeto(hour):
	"""One-hot of vector time"""
	time_vector = [0]*24
	time_vector[hour] = 1
	return time_vector

def weekto(weekday):
	"""One-hot of weekday"""
	week_vector = [0] * 7
	week_vector[weekday] = 1
	return week_vector

def monthto(months, month):
	"""One-hot of month"""
	month_index = months.index(month)
	month_vec = [0] * 12
	month_vec[month_index] = 1
	return month_vec	 

def backend_predict(list_of_doctors_current, time_military_format, month_vector, weekday_vec, model, list_of_doctors_total):
	"""
	TODO update with weekday implementation
	Used for the PPH calculator
	Predict PPH value based on list of doctors, time, and month
	"""
	doctor_vector = vectorize_doctors(list_of_doctors_current=list_of_doctors_current, list_of_doctors_total=list_of_doctors_total)
	time_vector = timeto(time_military_format)
	combined_vector = doctor_vector + time_vector + month_vector + weekday_vec
	combined_vector = np.array([combined_vector])
	prediction = model.predict(np.array(combined_vector))
	return prediction

def calculate_bg_color_with_PIP(pph_sum, pi_sum):
	if (1.1 * pi_sum) > pph_sum >= (0.9 * pi_sum): background_color = ('#19D529')			# Green
	elif (0.9 * pi_sum) > pph_sum >= (0.8 * pi_sum): background_color = ('#FFD400')			# Yellow
	elif pph_sum >= (1.1 * pi_sum): background_color = ("#4f95ff")							# Blue
	else: background_color = ('#FF4900')													# Red 
	return background_color

def calculate_bg_color_with_PIA(pia_sum):
	if pia_sum >= 120: bgcolor = '#FF4900'		#Red: Waiting Times Over 2h
	elif pia_sum >= 90: bgcolor = '#FFD400'		#Yellow: Waiting Times Over 1h30 and under 2h
	elif pia_sum > 45: bgcolor = '#19D529'		#Green: Waiting Times over 45m and under 1h30
	else: bgcolor = '#4f95ff'					#Blue: Waiting Times below 45m
	return bgcolor

def calculate_overall_score_with_PIP(pph_list, pi_list, green_threshold=0.95, red_threshold=0.80, blue_threshold=1.2):
	"""
	Returns list with color scoring based on threshould
	""" 
	Overall = []	# list containing color codes
	counter = range(len(pph_list))
	for pph, pi, count in zip(pph_list, pi_list, counter):

		# if PPH is over 0.95*PI values, then append green code
		if blue_threshold * pi > pph >= (green_threshold * pi): Overall.append('Green')

		# if there is overstaffing
		elif pph >= blue_threshold * pi: Overall.append("Blue")

		# elif PPH is smaller than PI, but is during the last 3 hours, then append orange color code
		elif pph < pi and count == 21 or pph < pi and count == 22 or pph < pi and count == 23: Overall.append('Orange')

		# elif PPH is superior than PI between 0.80 and 0.95, then append yellow code
		elif pph >= (red_threshold * pi): Overall.append('Yellow')

		# otherwise if PPH is inferior than 0.80*PI, then append red code
		else: Overall.append('Red')

	return Overall


def vectorize_row(date, day_schedule, month_vec, PPHhour, PIhour, PIAhour, PPHday, PIday, PIAday, list_of_doctors_total_csv):
	"""
	Returns PPH list and PI list based on day_schedule and PPH and PI predictions

	PPHhour: PPH predictor
	PIhour: PI predictor
	"""
	pph_list = []
	pi_list = []
	pia_list = []
	weekday_vec = weekto(datetime.datetime.strptime(date, "%Y-%m-%d").weekday())
	all_doctors = []
	for index, row in day_schedule.iterrows():
		list_of_doctors_per_row = []
		for i, doctor in enumerate(row):
			if pd.isnull(doctor) == False:
				list_of_doctors_per_row.append(doctor)
				if doctor not in all_doctors: all_doctors.append(doctor)

		# vectorize (one-hot) input data
		vectorized_row_doctors = vectorize_doctors(list_of_doctors_per_row, list_of_doctors_total_csv)
		vectorized_hour = timeto((index + 7) % 24)
		vector1 = vectorized_row_doctors + vectorized_hour + month_vec + weekday_vec
		vector2 = vectorized_hour + month_vec + weekday_vec

		# make prediction
		PPH_prediction = PPHhour.predict(np.array([vector1]))		# PPH per hour
		PI_prediction = PIhour.predict(np.array([vector2]))			# PI per hour
		PIA_prediction = PIAhour.predict(np.array([vector1]))		# PIA per hour

		pph_list.append(PPH_prediction)
		pi_list.append(PI_prediction)
		pia_list.append(PIA_prediction)

	vectorized_day_doctors = vectorize_doctors(all_doctors, list_of_doctors_total_csv)
	vector3 = vectorized_day_doctors + month_vec + weekday_vec
	vector4 = month_vec + weekday_vec

	day_PPH_prediction = np.array(PPHday.predict(np.array([vector3]))).flatten()[0]  #PPH per day
	day_PI_prediction = np.array(PIday.predict(np.array([vector4]))).flatten()[0]	 #PI per day
	day_PIA_prediction = np.array(PIAday.predict(np.array([vector3]))).flatten()[0]	 #PIA per day

	#print(str(int(day_PIA_prediction / 60)) + 'h ' + str(int(day_PIA_prediction % 60)) + 'min')

	pph_list = np.array(pph_list).flatten()
	pi_list = np.array(pi_list).flatten()
	pia_list = np.array(pia_list).flatten()
	pph_list = [round(value, 1) for value in pph_list]
	pi_list = [round(value, 1) for value in pi_list]
	pia_list = [round(value, 0) for value in pia_list]

	#day_PPH_prediction = round(sum(pph_list))
	#day_PI_prediction = round(sum(pi_list))

	return pph_list, pi_list, pia_list, day_PPH_prediction, day_PI_prediction, day_PIA_prediction


def get_calendars(search_date ,search_date_end, months):
	"""Creates all HTML calendars"""

	#Getting calendar months

	calendar_dict = {}
	calendar_months = []
	all_calendar_days = []
	html_calendars = []

	for dt in daterange(search_date, search_date_end):
		calendar_months.append(dt.strftime("%Y-%m"))
		all_calendar_days.append(dt.strftime("%Y-%m-%d"))

	unique_months = sorted(list(set(calendar_months)))
	for var in unique_months:
		m = months[int(var.split('-')[1].strip()) - 1] + var.split('-')[0].strip()
		calendar_dict[m] = calendar_months.count(var)
				
	for date in unique_months:
		year = int(date.split('-')[0].strip())
		month = int(date.split('-')[1].strip())
		raw = calendar.HTMLCalendar(calendar.SUNDAY)
		raw_calendar = raw.formatmonth(year, month)
		html_calendars.append(raw_calendar)

	return calendar_dict, all_calendar_days, html_calendars


def configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays):
	"""
	Generate color calendar
	"""
	originals = []
	replacements = []

	for month, length in calendar_dict.items():
		original_days = []
		new_days = []

		important_ids = all_ids[:length]
		important_days = all_calendar_days[:length]
		important_backgrounds = background_color[:length]

		for index, date in enumerate(important_days):
			y, m, day = (int(x) for x in date.split('-')) 
			weekday = weekdays[datetime.date(y, m, day).weekday()]
			bgcolor = important_backgrounds[index]
			id_ = important_ids[index]

			original_days.append(f"""<td class="{weekday}">{day}</td>""")
			new_days.append(f"""<td id="{id_}" bgcolor="{bgcolor}" class="{weekday}"><button class="table" type="button">{day}</button></td>""")

		originals.append(original_days)
		replacements.append(new_days)

		all_calendar_days = all_calendar_days[length:]
		background_color = background_color[length:]
		all_ids = all_ids[length:]

	return originals, replacements		

def configure_html_calendars(originals, replacements, html_calendars):
	"""Generate HTML calendar
	Relies on configure_calendars()"""
	for index, cal in enumerate(html_calendars):
		original_string = """<table border="0" cellpadding="0" cellspacing="0" class="month">"""
		new_string = """<table style="width=100%; table-layout:fixed;" class="table table-bordered">"""
		cal = cal.replace(original_string, new_string)

		orig = originals[index]
		repl = replacements[index]

		for i, o in enumerate(orig):
			r = repl[i]
			cal = cal.replace(o, r)

		html_calendars[index] = cal	
	return html_calendars	

def add_carousel_to_html_calendar_table(raw_html_table_list):
	"""This function adds Bootstrap 4 carousel to calendars"""
	count = 0
	html_table_with_carousel = ''
	html_table_with_carousel += """<div id="carouselExampleControls" class="carousel slide" data-ride="carousel"> <div class="carousel-inner">"""
	for item in raw_html_table_list:
		if count == 0:
			temp = """<div class="carousel-item active">""" + item 
		else:
			temp = """<div class="carousel-item">""" + item
		temp = temp + "</div>"
		html_table_with_carousel += temp
		count += 1
	html_table_with_carousel += """</div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>"""
	return html_table_with_carousel


def feature_importance(list_of_doctors_total_csv, raw_table, month_vector, weekday_vec, model):
	"""Color code text"""
	for index, row in raw_table.iterrows():
		#Time Vector
		time_vector = timeto((index+7) % 24)

		# remove columns that aren't needed
		doctor_row = row.drop(labels=['Time', 'PPH score', 'Overall score', 'Admin on call', 'Backup day', 'Backup night'])
		doctor_vector = [0] * len(list_of_doctors_total_csv)
		for doctor_name in doctor_row:
			doctor_last_name = doctor_name.split(',')[0]
			if any(doctor_last_name.lower() == doctor_full_name.lower().split(',')[0] for doctor_full_name in list_of_doctors_total_csv):
				doctor_index = list_of_doctors_total_csv.index(doctor_name)
				doctor_vector[doctor_index] = 1

		#If number of doctors in row is equal to 0
		if doctor_vector.count(1) == 0:
			new_doctor_name = 'No doctor registered'
			new_row = row.replace(doctor_name, new_doctor_name)
			raw_table.ix[index] = new_row

		#If number of doctors in row is equal to 1
		elif doctor_vector.count(1) == 1 and row['Overall score'] == 'Orange':
			new_doctor_name = '!' + doctor_name +'!!'
			new_row = row.replace(doctor_name, new_doctor_name)
			raw_table.ix[index] = new_row
		
		#If number of doctors is superior to 1	
		elif row['Overall score'] == 'Red':
			input_vector = doctor_vector + time_vector + month_vector + weekday_vec
			doctor_weights = process_weights(input_vector, model, list_of_doctors_total_csv)

			for doctor, weight in doctor_weights.items():
				if weight > 0:
					new_doctor_name = '!' + str(doctor) + '!!'
					row.replace(doctor, new_doctor_name, inplace=True)

			highest = max(doctor_weights.items(), key=operator.itemgetter(1))[0]
			new_doctor_name = '!' + str(highest) +'!!'
			row.replace(highest, new_doctor_name, inplace=True)

			raw_table.ix[index] = row

		elif row['Overall score'] == 'Yellow':
			input_vector = doctor_vector + time_vector + month_vector + weekday_vec
			doctor_weights = process_weights(input_vector, model, list_of_doctors_total_csv)
			
			for doctor, weight in doctor_weights.items():
				if weight > 0:
					new_doctor_name = '?' + str(doctor) + '??'
					row.replace(doctor, new_doctor_name, inplace=True)

			highest = max(doctor_weights.items(), key=operator.itemgetter(1))[0]
			new_doctor_name = '?' + str(highest) +'??'
			row.replace(highest, new_doctor_name, inplace=True)

			raw_table.ix[index] = row	

	return raw_table			


def update_BB_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, total_hours, doctor_shifts, shift_types):
	shiftType = shift_types[shift]
	if shiftType == 'backup': backup_shifts[doctor] += 1
	elif shiftType == 'day': day_shifts[doctor] += 1
	elif shiftType == 'night': night_shifts[doctor] += 1

	if shift not in ('Admin on call', 'Backup night', 'Backup day'): total_hours[doctor] += (doctor_shifts[shift][1] - doctor_shifts[shift][0])
	if datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 5 or datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 6: weekends[doctor] += 1


def schedule_df_to_html(day_schedule):
	"""Convert day schedule to HTML table with color coding and various formatting"""
	html_table = day_schedule.to_html(index=False)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width:auto; table-layout:fixed;" class="table table-bordered">"""


	# format HTML table content
	html_table = html_table.replace(original_string, new_string)
	html_table = html_table.replace("""!""", """<p>""")
	html_table = html_table.replace("""!!""", """</p>""")
	html_table = html_table.replace("""?""", """<p class=yellow>""")
	html_table = html_table.replace("""??""", """</p>""")

	# replacing keywords with HEX colors
	html_table = html_table.replace("""<th>Time</th>""", """<th width=90>Time</th>""")
	html_table = html_table.replace("""<td>Green</td>""", """<td class='green'></td>""")
	html_table = html_table.replace("""<td>Yellow</td>""", """<td class='yellow'></td>""")
	html_table = html_table.replace("""<td>Red</td>""", """<td class='red'></td>""")	
	html_table = html_table.replace("""<td>Orange</td>""", """<td class='orange'></td>""")
	html_table = html_table.replace("""<td>Blue</td>""", """<td class='blue'></td>""")

	return html_table

def return_list_of_indices_ascending_order_pph_minus_pi(pph_minus_pi_list):
	"""Function returns list of indices of days based on their (PPH - PI) values in ascending order"""
	copy_of_pph_minus_pi_list = pph_minus_pi_list.copy()
	list_ascending_order = []
	while len(copy_of_pph_minus_pi_list) > 0:
		min_of_copy_of_pph_minus_pi_list = min(copy_of_pph_minus_pi_list)
		worst_days_pph_minus_pi_index = [index for index, value in enumerate(pph_minus_pi_list) if value == min_of_copy_of_pph_minus_pi_list]
		for index in worst_days_pph_minus_pi_index:
			list_ascending_order.append(index)
		while min_of_copy_of_pph_minus_pi_list in copy_of_pph_minus_pi_list:
			copy_of_pph_minus_pi_list.remove(min_of_copy_of_pph_minus_pi_list)
		# copy_of_pph_minus_pi_list[:] = [x for x in copy_of_pph_minus_pi_list if x!=min_of_copy_of_pph_minus_pi_list]
	return list_ascending_order


def calculate_doctor_shifts_plus_minus_per_day(pph_minus_pi, doctor_pphpd=2.0, doctor_hours_per_shift=8.0):
	"""
	Function computes the number of shifts to add or to remove for any given day
	number_of_shifts = -(patients per hour - patient inflow) / (patients per hour per doctor * number of hours per shift)
	returns rounded result of number of shifts
	"""
	shifts_plus_minus = round(-(pph_minus_pi)/(doctor_pphpd*doctor_hours_per_shift))
	return shifts_plus_minus
