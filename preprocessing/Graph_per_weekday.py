import pandas as pd
import datetime
import matplotlib.pyplot as plt

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

PPH_dates = []
PI_dates = []

for index, row in df_data.iterrows():
	PI_dates.append(str(row['Arrival']).split(' ')[0])
	PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0])

df_data['PPH_dates'] = PPH_dates
df_data['PI_dates'] = PI_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

PPH_weekday_data = []
PI_weekday_data = []
PIA_weekday_data = []
NOD_weekday_data = []

for weekday in range(0, 7):
	current_dates = [x for x in unique_dates if datetime.datetime.strptime(x, "%Y-%m-%d").weekday() == weekday]
	current_PPH_data = []
	current_PIA_data = []
	current_PI_data = []
	current_NOD_data = []
	for date_code in current_dates:
		PPH_selection = df_data.ix[df_data['PPH_dates'] == date_code]
		PI_selection = df_data.ix[df_data['PI_dates'] == date_code]
		PIA_times = [((row['PIA_NPIA'] - row['Arrival']).total_seconds()/3600) for index, row in PI_selection.iterrows()]
		unique_doctors = len(list(set(list(PPH_selection['StaffMD']))))

		current_NOD_data.append(unique_doctors)
		current_PIA_data.append(sum(PIA_times) / len(PIA_times))
		current_PPH_data.append(len(PPH_selection))
		current_PI_data.append(len(PI_selection))

	NOD_weekday_data.append(current_NOD_data)
	PIA_weekday_data.append(current_PIA_data)
	PPH_weekday_data.append(current_PPH_data)
	PI_weekday_data.append(current_PI_data)

NOD_mean = [(sum(x) / len(x)) for x in NOD_weekday_data]
PIA_mean = [(sum(x) / len(x)) for x in PIA_weekday_data]
PPH_mean = [(sum(x) / len(x)) for x in PPH_weekday_data]
PI_mean = [(sum(x) / len(x)) for x in PI_weekday_data]

weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
x = range(0, 7)

text_file = open("Weekday_PIA_Average.txt", "w")
for x in PIA_mean:
	text_file.write(str(x) + ' ')
text_file.close()

'''
plt.xticks(x, weekdays)
for xe, ye in zip(x, NOD_weekday_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, NOD_mean, c='g', s=60, label='Valeur moyenne')
plt.title('Nombre de médecins au travail selon le jour de la semaine')
plt.legend()
plt.xlabel('Jour de la semaine')
plt.ylabel('Nombre de médecins')
plt.savefig('fr-NumOfDocs per day per weekday', dpi=500)
plt.close()


plt.xticks(x, weekdays)
for xe, ye in zip(x, PIA_weekday_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PIA_mean, c='g', s=60, label='Valeur moyenne')
plt.title("Temps d'attente moyen selon le jour de la semaine")
plt.legend()
plt.xlabel('Jour de la semaine')
plt.ylabel('Heures')
plt.savefig('fr-average PIA per day per weekday', dpi=500)
plt.close()


plt.xticks(x, weekdays)
for xe, ye in zip(x, PPH_weekday_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PPH_mean, c='r', s=60, label='Valeur moyenne')
plt.title('Nombre de patients vus selon le jour de la semaine')
plt.legend()
plt.xlabel('Jour de la semaine')
plt.ylabel('Nombre de patients vus')
plt.savefig('fr-PPH per day per weekday', dpi=500)
plt.close()

plt.xticks(x, weekdays)
for xe, ye in zip(x, PI_weekday_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PI_mean, c='b', s=60, label='Valeur moyenne')
plt.legend()
plt.title('Afflux de patients selon le jour de la semaine')
plt.xlabel('Jour de la semaine')
plt.ylabel('Nombre de patients')
plt.savefig('fr-PI per day per weekday', dpi=500)
plt.close()
'''