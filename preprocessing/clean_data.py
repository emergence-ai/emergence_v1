import pandas as pd
import numpy as np
import os 
import datetime


#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()


def inverse_day_month(df_data):

	for index, row in df_data.iterrows():

		Arrival = row['Arrival']
		PIA_NPIA = row['PIA_NPIA']
		DischargeTime = row['DischargeTime']

		PIA_Time = (PIA_NPIA - Arrival).total_seconds() / 3600 

		if 600 < PIA_Time < 800:
			print(index)
			PIA_NPIA = str(PIA_NPIA)
			PIA_NPIA_date = PIA_NPIA.split(' ')[0]
			PIA_NPIA_time = PIA_NPIA.split(' ')[1]

			PIA_NPIA_year = PIA_NPIA_date.split('-')[0].strip()
			PIA_NPIA_two = PIA_NPIA_date.split('-')[1].strip()
			PIA_NPIA_three = PIA_NPIA_date.split('-')[2].strip()


			DischargeTime = str(DischargeTime)
			DischargeTime_date = DischargeTime.split(' ')[0]
			DischargeTime_time = DischargeTime.split(' ')[1]

			DischargeTime_year = DischargeTime_date.split('-')[0].strip()
			DischargeTime_two = DischargeTime_date.split('-')[1].strip()
			DischargeTime_three = DischargeTime_date.split('-')[2].strip()


			new_PIA_NPIA = (PIA_NPIA_year + '-' + PIA_NPIA_three + '-' + PIA_NPIA_two) + ' ' + PIA_NPIA_time
			new_PIA_NPIA = datetime.datetime.strptime(new_PIA_NPIA, "%Y-%m-%d %H:%M:%S")

			new_DischargeTime = (DischargeTime_year + '-' + DischargeTime_three + '-' + DischargeTime_two) + ' ' + DischargeTime_time
			new_DischargeTime = datetime.datetime.strptime(new_DischargeTime, "%Y-%m-%d %H:%M:%S")

			row = row.replace(PIA_NPIA, new_PIA_NPIA)
			row = row.replace(DischargeTime, new_DischargeTime)

		df_data.ix[index] = row	

	return df_data



df_data = inverse_day_month(df_data)



'''
def inverse_year(date_time):
	date = date_time.split(' ')[0]
	time = date_time.split(' ')[1]
	p = date_time.split(' ')[2]
	month = date.split('-')[0].strip()
	two = date.split('-')[1].strip()
	three = date.split('-')[2].strip()

	if two == '17':
		date_time = (month + '-' + three + '-' + two) + ' ' + time + ' ' + p
	elif three == '17':
		date_time = (month + '-' + two + '-' + three) + ' ' + time + ' ' + p
	return date_time

def add_padding(date_time):
	date = date_time.split(' ')[0]
	time = date_time.split(' ')[1]
	p = date_time.split(' ')[2]

	if len(date.split('-')[0]) == 1:
		date = '0' + date

	if len(time.split(':')[0]) == 1:
		time = '0' + time

	date_time = str(date + ' ' + time + ' ' + p)
	new_datetime = datetime.datetime.strptime(date_time, "%m-%d-%y %I:%M %p") 
	return new_datetime

Arrival = [add_padding(inverse_year(x)) for x in list(df_data['Arrival'])]
PIA_NPIA = [add_padding(inverse_year(x)) for x in list(df_data['PIA_NPIA'])]
DischargeTime = [add_padding(inverse_year(x)) for x in list(df_data['DischargeTime'])]
Time = [add_padding(inverse_year(x)) for x in list(df_data['Time'])]

df_data['Arrival'] = Arrival
df_data['PIA_NPIA'] = PIA_NPIA
df_data['DischargeTime'] = DischargeTime
df_data['Time'] = Time
'''

df_data = df_data.sort_values(by='Arrival', ascending=True)
df_data.to_excel('2017_clean_data.xlsx')
