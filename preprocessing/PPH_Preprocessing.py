import pandas as pd
import numpy as np
import string
import os 


# Read Dataset
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()

#Get Unique List Of Doctors In Dataset

list_of_doctors = list(df_data["StaffMD"].unique())
print(len(list_of_doctors))


X = []      # training dataset  [vector of doctors per 1-hour block]
y = []      # PPH time  [average PPH]

# Process data
processed_dataset = []
for row in range(df_data.shape[0]):
    processed_temp_row = []
    processed_temp_row.append(df_data["PIA_NPIA"][row])
    processed_temp_row.append(df_data["StaffMD"][row])
    processed_dataset.append(processed_temp_row)

doctors_present = []                       # list of doctors present      
doctors_vector = [0] * len(list_of_doctors)

date_codes = []
hour_codes = []
month = [] 

for row in processed_dataset:
    day = str(row['PIA_NPIA'].day)
    if len(day) == 1:
        day = '0' + str(row['PIA_NPIA'].day)

    # ###################### 1 HOUR BLOCK ########################################
    # number_of_hours_per_block = 1   ### 1 HOUR (COMMENT!) TODO CHAMGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)

    number_of_hours_per_block = 1    # TODO CHAMGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)
    current_date = str(row['PIA_NPIA'].month) + '-' + day + '-' + str(row['PIA_NPIA'].year) + ' ' + str(int(row['PIA_NPIA'].hour/number_of_hours_per_block))
    date_codes.append(current_date)
    hour_codes.append(int(row['PIA_NPIA'].hour / number_of_hours_per_block))

date_codes = np.array(date_codes)
hour_codes = np.array(hour_codes)

df_data['date_codes'] = date_codes
df_data['hour_codes'] = hour_codes

date_codes_unique = np.sort(np.unique(date_codes))            #Getting unique date/hour to group rows per hour block

#Patients per hour per doctor = PPH / number of doctors
listed_PPHPD = []

# for unique date codes
for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]         #Grouping rows per unique date/hour block_hours

    doctors_vector = [0] * len(list_of_doctors)						      #Defining one hot vector of doctors

    for df_row in df_selection.iterrows():
        index, row = df_row

        # compute doctor vector
        for doctor_index in range(len(list_of_doctors)):
            if list_of_doctors[doctor_index] == row["StaffMD"]:
                doctors_vector[doctor_index] = 1    					  #Setting active doctors to 1

        hour = row["hour_codes"]
        Weekday = row['PIA_NPIA'].weekday()

    # initialize hour vector
    hour_vector = [0] * 24
    hour_vector[hour] = 1

    #Weekday vec
    weekday_vec = [0] * 7
    weekday_vec[Weekday] = 1

    # initialize vectors for the month
    month_vec = [0] * 12
    month_int = int(date_code.split('-')[0])
    month_vec[month_int - 1] = 1

    # Add vectors to training data
    X.append(doctors_vector + hour_vector + month_vec)
    
    tp=(float(len(df_selection)))                                        #Defining patients per hour (PPH) by taking the number of rows. 
    PPHPD = tp / doctors_vector.count(1)
    listed_PPHPD.append(PPHPD)

    y.append(tp)

#Exponentiating Target Values
data_average_and_median = 2.67
exp_values = []
for value in listed_PPHPD:
    new_value = value - data_average_and_median
    if new_value >= 0:
        exp_value = (new_value ** 2) + data_average_and_median
        exp_values.append(exp_value)
    elif new_value < 0:
        exp_value = -(new_value ** 2) + data_average_and_median
        exp_values.append(exp_value)


# convert lists to NumPy array
X = np.array(X)
y = np.array(y)


# save training and testing data as CSV files

np.savetxt("X_train_22-08-18.csv", X, delimiter=",")
np.savetxt("y_train_22-08-18.csv", y, delimiter=',')


#Visualize some data
print(np.max(y.flatten()))
print(np.min(y.flatten()))
print(np.mean(y.flatten()))
print(np.std(y.flatten()))