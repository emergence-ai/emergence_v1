import pickle 
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression, SGDClassifier
from sklearn.metrics import mean_squared_error, log_loss
from sklearn.model_selection import learning_curve, train_test_split
import statsmodels.api as sm

with open("PIA_X_daily.txt", "rb") as fp:
	X_daily = pickle.load(fp)

with open("PIA_y_daily.txt", "rb") as fp:
	y_daily = pickle.load(fp)

with open("daily_summaries.txt", "rb") as fp:
	unique_daily_summaries = pickle.load(fp)

'''
X_daily = np.load('X_APIA_per_day.npy')
y_daily = np.load('y_APIA_per_day.npy')
'''
'''
X_daily = pd.read_csv('X_train_PIA_day_10-01-19.csv', sep=',', header=None)
y_daily = pd.read_csv('y_train_PIA_day_10-01-19.csv', sep=',', header=None)
'''
'''
del X_daily[0]
del y_daily[0]
del X_daily[1]
del y_daily[1]
del X_daily[7]
del y_daily[7]
del X_daily[10]
del y_daily[10]
'''


for index, list_ in enumerate(X_daily):
	for i, summary in enumerate(unique_daily_summaries):
		if summary in list_:
			list_[list_.index(summary)] = i


X_train, X_test, y_train, y_test = train_test_split(X_daily, y_daily, test_size=0.10, shuffle=True)

np.save('PIA_daily_X_train.npy', X_daily)
np.save('PIA_daily_y_train.npy', y_daily)

clf = LinearRegression(n_jobs=-1)
clf.fit(X_train, y_train)
print(clf.score(X_test, y_test))

predictions = clf.predict(X_test)
mse = mean_squared_error(y_test, predictions)
#loss = log_loss(y, predictions)
print(mse)
new_pred = clf.predict(X_daily)
mse = mean_squared_error(y_daily, new_pred)

print(mse)

'''
with open('PIA_per_day_WW.pickle', 'wb') as handle:
    pickle.dump(clf, handle, protocol=pickle.HIGHEST_PROTOCOL)
'''

model = sm.OLS(y_daily, X_daily).fit()
predictions = model.predict(X_daily) # make the predictions by the model
mse = mean_squared_error(y_daily, predictions)

print(mse)

# Print out the statistics
print(model.summary())

'''
with open('PIA_per_day.pickle', 'wb') as handle:
    pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)
'''