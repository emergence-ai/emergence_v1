#Process raw events

import pandas as pd 

raw_events = pd.read_excel("C:/Users/slu13/Desktop/raw_events.xlsx", header=0)

def hasNumbers(inputString):
	return any(char.isdigit() for char in inputString)

dateList = []
eventNames = []

for index, row in raw_events.iterrows():
	if row['Column1'] == "When" and hasNumbers(row['Column2']) == True:
		dateList.append(row['Column2'])
		eventNames.append(raw_events.ix[index - 2]['Column1'])


final_df = pd.DataFrame()
final_df['eventNames'] = eventNames
final_df['date'] = dateList

final_df.to_excel("events.xlsx")