import pandas as pd
import datetime
import matplotlib.pyplot as plt

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

PPH_dates = []
PI_dates = []
PPH_hours = []
PI_hours = []

for index, row in df_data.iterrows():
	PI_dates.append(str(row['Arrival']).split(' ')[0])
	PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0])
	PPH_hours.append(int(row['PIA_NPIA'].hour))
	PI_hours.append(int(row['Arrival'].hour))

df_data['PPH_dates'] = PPH_dates
df_data['PI_dates'] = PI_dates
df_data['PPH_hours'] = PPH_hours
df_data['PI_hours'] = PI_hours

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

PPH_hourly_data = []
PI_hourly_data = []
PIA_hourly_data = []
NOD_hourly_data = []

for hour in range(0, 24):
	print(hour)
	current_PPH_data = []
	current_PIA_data = []
	current_PI_data = []
	current_NOD_data = []

	for i, date_code in enumerate(unique_dates):
		print(i)
		PPH_selection = df_data.ix[(df_data['PPH_hours'] == hour) & (df_data['PPH_dates'] == date_code)]
		PI_selection = df_data.ix[(df_data['PI_hours'] == hour) & (df_data['PI_dates'] == date_code)]
		PIA_times = [((row['PIA_NPIA'] - row['Arrival']).total_seconds() / 3600) for index, row in PI_selection.iterrows()]
		for index, piat in enumerate(PIA_times): 
			if piat < 0: 
				print(PI_selection.iloc[index]['PtAccnt'])
				print(date_code, hour)
				raise ValueError
		unique_doctors = len(list(set(list(PPH_selection['StaffMD']))))

		if len(PIA_times) == 0: current_PIA_data.append(0)
		else: current_PIA_data.append(sum(PIA_times) / len(PIA_times))
		current_PPH_data.append(len(PPH_selection))
		current_PI_data.append(len(PI_selection))
		current_NOD_data.append(unique_doctors)

	NOD_hourly_data.append(current_NOD_data)
	PIA_hourly_data.append(current_PIA_data)
	PPH_hourly_data.append(current_PPH_data)
	PI_hourly_data.append(current_PI_data)

NOD_mean = [(sum(x) / len(x)) for x in NOD_hourly_data]
PIA_mean = [(sum(x) / len(x)) for x in PIA_hourly_data]
PPH_mean = [(sum(x) / len(x)) for x in PPH_hourly_data]
PI_mean = [(sum(x) / len(x)) for x in PI_hourly_data]


text_file = open("Hour_PIA_Average.txt", "w")
for x in PIA_mean:
	text_file.write(str(x) + ' ')
text_file.close()
'''
text_file = open("Hour_PI_Average.txt", "w")
for x in PI_mean:
	text_file.write(str(x) + ' ')
text_file.close()
'''
'''
hours = ['12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm' ,'4pm', '5pm' ,'6pm', '7pm', '8pm', '9pm', '10pm', '11pm']
x = range(0, 24)

plt.xticks(x, hours, fontsize=8, rotation=90)
for xe, ye in zip(x, NOD_hourly_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, NOD_mean, c='g', s=60, label='Valeur moyenne')
plt.title('Nombre de médecins au travail selon le temps du jour')
plt.legend()
plt.xlabel('Temps du jour')
plt.ylabel('Nombre de docteurs')
plt.savefig('fr-NumOfDocs per time of day', dpi=500)
plt.close()

plt.xticks(x, hours, fontsize=8, rotation=90)
for xe, ye in zip(x, PIA_hourly_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PIA_mean, c='g', s=60, label='Valeur moyenne')
plt.title("Temps d'attente moyen groupé selon le temps du jour")
plt.legend()
plt.xlabel("Temps du jour")
plt.ylabel("Heures d'attente")
plt.savefig('fr-average PIA per hour per time of day', dpi=500)
plt.close()

plt.xticks(x, hours, fontsize=8, rotation=90)
for xe, ye in zip(x, PPH_hourly_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PPH_mean, c='r', s=60, label='Valeur moyenne')
plt.title("Nombre de patients vus selon le temps du jour")
plt.legend()
plt.xlabel('Temps du jour')
plt.ylabel('Nombre de patients vus')
plt.savefig('fr-PPH per hour per time of day', dpi=500)
plt.close()

plt.xticks(x, hours, fontsize=8, rotation=90)
for xe, ye in zip(x, PI_hourly_data):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(x, PI_mean, c='b', s=60, label='Valeur moyenne')
plt.title('Afflux de patients selon le temps du jour')
plt.legend()
plt.xlabel('Temps du jour')
plt.ylabel('Nombre de patients')
plt.savefig('fr-PI per hour per time of day', dpi=500)
plt.close()
'''