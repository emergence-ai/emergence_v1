import pandas as pd
import datetime
import matplotlib.pyplot as plt

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

PPH_dates = []
PI_dates = []

for index, row in df_data.iterrows():
	PI_dates.append(str(row['Arrival']).split(' ')[0])
	PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0])

df_data['PPH_dates'] = PPH_dates
df_data['PI_dates'] = PI_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

PI_data = []
NOD_data = []
PIA_data = []

for date_code in unique_dates:
	PI_selection = df_data.ix[df_data['PI_dates'] == date_code]
	PIA_times = [((row['PIA_NPIA'] - row['Arrival']).total_seconds() / 3600) for index, row in PI_selection.iterrows()]
	unique_doctors = len(list(set(list(PI_selection['StaffMD']))))

	PI_data.append(len(PI_selection))
	NOD_data.append(unique_doctors)
	PIA_data.append(sum(PIA_times) / len(PIA_times))

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
x = range(0, 365, 32)
plt.xticks(x, months, fontsize=9, rotation=90)
plt.plot(range(len(unique_dates)), PI_data, c='b', label='Patient inflow volume')
plt.title('Patient inflow in 2017')
plt.legend()
plt.xlabel('Month')
plt.ylabel('Patients')
plt.savefig('PI_2017', dpi=500)
plt.close()

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
x = range(0, 365, 32)
plt.xticks(x, months, fontsize=9, rotation=90)
plt.plot(range(len(unique_dates)), NOD_data, c='k', label='Number of doctors on shift')
plt.title('Number of docs on shift in 2017')
plt.legend()
plt.xlabel('Month')
plt.ylabel('Doctors')
plt.savefig('NOD_2017', dpi=500)
plt.close()

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
x = range(0, 365, 32)
plt.xticks(x, months, fontsize=9, rotation=90)
plt.plot(range(len(unique_dates)), PIA_data, c='g', label='Average wait time')
plt.title('Average wait time in 2017')
plt.legend()
plt.xlabel('Month')
plt.ylabel('Hours')
plt.savefig('PIA_2017', dpi=500)
plt.close()