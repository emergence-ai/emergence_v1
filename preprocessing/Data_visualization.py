import pandas as pd
import numpy as np
import os 
import datetime
import matplotlib.pyplot as plt

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)


PI_list = []


date_codes = []
for index, row in df_data.iterrows():
    day = str(row['PIA_NPIA'].day)
    if len(day) == 1:
        day = '0' + str(row['PIA_NPIA'].day)

    number_of_hours_per_block = 1
    current_date = str(row['PIA_NPIA'].month) + '-' + day + '-' + str(row['PIA_NPIA'].year) + ' ' + str(int(row['PIA_NPIA'].hour/number_of_hours_per_block))
    date_codes.append(current_date)


date_codes = np.array(date_codes)
df_data['date_codes'] = date_codes


date_codes_unique = np.sort(np.unique(date_codes)) 


for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]
    PI_list.append(len(df_selection))


unique_PI = sorted(list(set(PI_list)))
PI_count = []


for PI_value in unique_PI:
	PI_count.append(PI_list.count(PI_value))


unique_PI = np.array(unique_PI)
PI_count = np.array(PI_count)


#np.savetxt('unique_PI.txt', unique_PI)
#np.savetxt('PI_count.txt', PI_count)


plt.plot(unique_PI, PI_count, label="PPH")
plt.legend()
plt.title('Distribution of patients per hour data')
plt.xlabel('unique PPH values')
plt.ylabel('Count')
plt.savefig('PPH_data_visualization.png', dpi=500)
plt.close()

'''
plt.plot(range(len(PIA_list)), PIA_list, label="PIA")
plt.legend()
plt.xlabel('days')
plt.ylabel('Average PIA value per day')
plt.savefig('PIA_data_visualization.png', dpi=500)
plt.close()
'''
