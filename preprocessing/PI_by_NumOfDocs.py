import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='Arrival', ascending=True)

PPH_dates = []

for index, row in df_data.iterrows():
	PPH_dates.append(str(row['Arrival']).split(' ')[0])

df_data['PI_dates'] = PPH_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

doctor_data = []
PI_data = []

for date_code in unique_dates:
	df_selection = df_data.ix[df_data['PI_dates'] == date_code]

	unique_doctors = list(set(list(df_selection['StaffMD'])))
	number_of_doctors = len(unique_doctors)
	doctor_data.append(number_of_doctors)

	PI_data.append(len(df_selection))


total_doctor = list(set(doctor_data))
total_PI = []
for x in total_doctor:
	current_PI = []
	for number_docs, PI in zip(doctor_data, PI_data):
		if number_docs == x: current_PI.append(PI)

	total_PI.append(current_PI)

PI_mean = [(sum(x) / len(x)) for x in total_PI]



for xe, ye in zip(total_doctor, total_PI):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(total_doctor, PI_mean, c='g', s=60, label='Average value')


#plt.scatter(doctor_data, PI_data, c='b', s=30)
plt.plot(np.unique(total_doctor), np.poly1d(np.polyfit(total_doctor, PI_mean, 1))(np.unique(total_doctor)), c='k')
plt.title('Patient inflow according to number of doctors on shift')
plt.xlabel('Number of doctors on shift')
plt.ylabel('Patients')
plt.savefig('Patient inflow per number of docs', dpi=500)
plt.close()