#Compare Models
import numpy as np 
import pickle
import datetime
from sklearn.metrics import mean_squared_error

def mean_absolute_percentage_error(y_true, y_pred): 
	new_y_true = []
	new_y_pred = []
	for t, p in zip(y_true, y_pred):
		if t == 0:
			continue
		else: 
			new_y_pred.append(p)
			new_y_true.append(t)
	
	y_true, y_pred = np.array(new_y_true), np.array(new_y_pred)
	return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


'''
with open("C:/Users/slu13/Desktop/Model/Models/Weather/PIA_PER_day/PIA_per_day_WW.pickle", "rb") as handle:
	PIA_Regression = pickle.load(handle)

X_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PIA_Per_day_Weather_Data/X_train_weather.npy")
'''
y_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PIA_Per_Hour_Weather_Data/y_train_weather.npy")


mon_avg = open('C:/Users/slu13/Desktop/Model/dataset/Rolling_Average/Month_PIA_Average.txt', 'r')
wkd_avg = open('C:/Users/slu13/Desktop/Model/dataset/Rolling_Average/Weekday_PIA_Average.txt', 'r')
hr_avg = open('C:/Users/slu13/Desktop/Model/dataset/Rolling_Average/Hour_PIA_Average.txt', 'r')

mon_avg = [float(x.strip()) * 60 for x in mon_avg]
wkd_avg = [float(x.strip()) * 60 for x in wkd_avg]
hr_avg = [float(x.strip()) * 60 for x in hr_avg]


base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_PIA_dates = [base + datetime.timedelta(days=x) for x in range(0, 365)]
unique_PIA_days = [str(base + datetime.timedelta(days=x)) for x in range(0, 24) for base in unique_PIA_dates]


Rolling_Average = []

'''
for date_code in unique_PIA_dates:
	date_code = date_code.split(' ')[0]
	month = int(date_code.split('-')[1]) - 1
	weekday = datetime.datetime.strptime(date_code, "%Y-%m-%d").weekday()

	current_prediction = (mon_avg[month] + wkd_avg[weekday]) / 2
	Rolling_Average.append(current_prediction)
'''


for date_code in unique_PIA_days:
	current_day = int((date_code.split(' ')[1]).split(':')[0])
	current_prediction = hr_avg[current_day]
	Rolling_Average.append(current_prediction)

del Rolling_Average[-1]


mse = mean_squared_error(y_train, Rolling_Average)
mape = mean_absolute_percentage_error(y_train, Rolling_Average)

print(mse, mape)


'''
#score = PIA_Regression.score(X_train, y_train)
Predictions = PIA_Regression.predict(X_train)
mse = mean_squared_error(y_train, Predictions)
mape = mean_absolute_percentage_error(y_train, Predictions)

print(mse, mape)
'''
#print(mse)