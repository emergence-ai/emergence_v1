import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np
import statistics

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

PPH_dates = []

for index, row in df_data.iterrows():
	PPH_dates.append(str(row['Arrival']).split(' ')[0])

df_data['PI_dates'] = PPH_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

doctor_data = []
PIA_data = []
PI_data = []

for date_code in unique_dates:
	df_selection = df_data.ix[df_data['PI_dates'] == date_code]

	unique_doctors = list(set(list(df_selection['StaffMD'])))
	number_of_doctors = len(unique_doctors)
	doctor_data.append(number_of_doctors)

	PIA_times = [((row['PIA_NPIA'] - row['Arrival']).total_seconds() / 3600) for index, row in df_selection.iterrows()]
	PIA_data.append(sum(PIA_times) / len(PIA_times))

	PI_data.append(len(df_selection))

high_NOD = []
high_PI = []
med_PI = []
med_NOD = []
low_NOD = []
low_PI = []

for NumOfDocs, PIA_time, PI in zip(doctor_data, PIA_data, PI_data):
	if PIA_time > 2.0:
		high_NOD.append(NumOfDocs)
		high_PI.append(PI)

	elif PIA_time < 1.0:
		low_NOD.append(NumOfDocs)
		low_PI.append(PI)
	
	elif PIA_time >= 1.0 and PIA_time <= 2.0:
		med_NOD.append(NumOfDocs)
		med_PI.append(PI)
	
med_PPD = (sum(med_PI) / len(med_PI)) / (sum(med_NOD) / len(med_NOD))
high_PPD = (sum(high_PI) / len(high_PI)) / (sum(high_NOD) / len(high_NOD))
low_PPD = (sum(low_PI) / len(low_PI)) / (sum(low_NOD) / len(low_NOD))

high_std = statistics.stdev([x / y for x, y in zip(high_PI, high_NOD)])
med_std = statistics.stdev([x / y for x, y in zip(med_PI, med_NOD)])
low_std = statistics.stdev([x / y for x, y in zip(low_PI, low_NOD)])

print(high_PPD, med_PPD, low_PPD)

x = range(0, 3)
waittimes = ['PIA > 2hrs', '2hrs >= PIA >= 1hr', 'PIA < 1hr']

plt.xticks(x, waittimes)
plt.bar(x, [high_PPD, med_PPD, low_PPD], width=0.35, color='orange', yerr=[high_std, med_std, low_std])
plt.xlabel("Temps d'attente", fontsize=9)
plt.ylabel('Nombre de patients moyen à traiter par médecin', fontsize=9)
plt.title("Nombre moyen de patients à traiter par médecin selon divers temps d'attente", fontsize=9)
#plt.savefig('API_per_NOD', dpi=500)
#plt.close()
plt.show()
