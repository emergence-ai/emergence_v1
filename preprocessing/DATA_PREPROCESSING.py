import pandas as pd
import numpy as np
import datetime 
import time
import requests
import json
import pickle


def get_doctor_last_names(df):
	raw_names = list(df['EmergencyPhysician'])
	raw_last_names = []
	for name in raw_names:
		if name == None or name == '': raw_last_names.append(None)
		elif name != None: raw_last_names.append(str(name).split(' ')[0].strip())
	last_names = [x.replace(',', '').lower() for x in raw_last_names]
	return last_names

def create_new_dataframe(df, last_names):
	arrivalTime = list(df['StartOfVisit'])
	piaTime = list(df['Pia_Npia_606'])

	stampedArrivalTime = [datetime.datetime.strptime(str(dt), '%Y-%m-%d %H:%M:%S') for dt in arrivalTime]
	stampedPiaTime = []

	for time in piaTime:
		if time == None or time == '': stampedPiaTime.append(None)
		else:
			time = str(time)
			time = time.replace('T', ' ')
			time = time.replace('Z', '')
			stampedPiaTime.append(datetime.datetime.strptime(time, '%Y-%m-%d %H:%M:%S'))

	new_df = pd.Dataframe()
	new_df['Arrival'] = stampedArrivalTime
	new_df['PIA_NPIA'] = stampedPiaTime
	new_df['Holiday'] = list(df['Holiday'])
	new_df['StaffMD'] = last_names
	return new_df


# Hospital Data
df_data = pd.read_csv('Insert file path here', header=0).dropna()

list_of_doctors = get_doctor_last_names(df_data)

df_data = create_new_dataframe(df_data, list_of_doctors)
df_data = df_data.sort_values(by='Arrival', ascending=True)

list_of_doctors = sorted(list(set([x for x in list_of_doctors if x != None])))
list_of_holidays = sorted(list(set(df_data['Holiday'])))

PPH_X_daily = []     
PPH_y_daily = []
PPH_X_hourly = []
PPH_y_hourly = []

PI_X_daily = []
PI_y_daily = []
PI_X_hourly = []
PI_y_hourly = []     

PIA_X_daily = []
PIA_y_daily = []
PIA_X_hourly = []
PIA_y_hourly = []

#DarkSky_url = 'https://api.darksky.net/forecast/9fb9bdf16e266d65f484e15278733853/43.70011,-79.4163,{}'

PI_dates = []
PPH_dates = []
PPH_datetimes = []
PI_datetimes = []

for index, row  in df_data.iterrows():
	PI_dates.append(str(row['Arrival']).split(' ')[0].strip())
	PI_datetimes.append(str(row['Arrival']).split(' ')[0].strip() + ' ' + str(row['Arrival'].hour).strip())
	if row['PIA_NPIA'] != None:
		PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0].strip())
		PPH_datetimes.append(str(row['PIA_NPIA']).split(' ')[0].strip() + ' ' + str(row['PIA_NPIA'].hour).strip())
	else: 
		PPH_dates.append(None)
		PPH_datetimes.append(None)

df_data['PPH_dates'] = PPH_dates
df_data['PPH_datetimes'] = PPH_datetimes
df_data['PI_dates'] = PI_dates
df_data['PI_datetimes'] = PI_datetimes

base = min(list(df_data['Arrival'])).date()
end = max(list(df_data['Arrival'])).date()
delta = end - base
unique_PI_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(delta.days + 1)]

#unique_hourly_summaries = []
#unique_daily_summaries = []

for date_code in unique_PI_dates:
	print(date_code)
	df_PI_selection = df_data.ix[df_data['PI_dates'] == date_code]
	df_PPH_selection = df_data.ix[df_data['PPH_dates'] == date_code]

	doctor_vec = [0] * len(list_of_doctors)
	PIA_times = []
	for index, row in df_PPH_selection.iterrows():
		doctor_vec[list_of_doctors.index(row['StaffMD'])] = 1
		PIA_times.append((row['PIA_NPIA'] - row['Arrival']).total_seconds())  

	average_PIA = sum(PIA_times) / len(PIA_times)
	PIA_y_daily.append(average_PIA / 60)

	#Pull daily weather data
	current_time = datetime.datetime.strptime(date_code, "%Y-%m-%d")

	'''
	timestamp = str(int(time.mktime(current_time.timetuple())) + 60)
	request_url = DarkSky_url.format(timestamp)
	response = requests.get(request_url)
	weather_data = response.json()
	'''
	'''
	#Daily weather data
	data = weather_data['daily']['data'][0]
	unique_daily_summaries.append(data['summary'])
	weather_vec = [data['summary'], data['temperatureHigh'], data['temperatureLow'], data['uvIndex'], data['precipIntensity'], data['humidity'], data['windSpeed'], data['visibility'], data['temperatureMax'] - data['temperatureMin'], data['dewPoint']]
	'''

	# initialize vector for the month
	month_vec = [0] * 12
	month = int(date_code.split('-')[1])
	month_vec[month - 1] = 1

	#Initialize vector for weekday
	weekday_vec = [0] * 7
	weekday = current_time.weekday()
	weekday_vec[weekday] = 1

	# initialize holiday vector
	holiday_vec = [0] * len(list_of_holidays)
	holiday = df_PI_selection.ix[0]['Holiday']
	holiday_vec[list_of_holidays.index(holiday)] = 1

	input_vec = doctor_vec + holiday_vec + month_vec + weekday_vec
	input_vec1 = holiday_vec + month_vec + weekday_vec

	PIA_X_daily.append(input_vec)

	PI_X_daily.append(input_vec1)
	PI_y_daily.append(float(len(df_PI_selection)))

	PPH_X_daily.append(input_vec)
	PPH_y_daily.append(float(len(df_PPH_selection)))



	for hour in range(0, 24):
		print(hour)
		datetime_code = date_code + ' ' + str(hour)
		PI_selection = df_data.ix[df_data['PI_datetimes'] == datetime_code]
		PPH_selection = df_data.ix[df_data['PPH_datetimes'] == datetime_code]
		all_PIA = []

		doctor_vec = [0] * len(list_of_doctors)
		for index, row in PPH_selection.iterrows():
			doctor_vec[list_of_doctors.index(row['StaffMD'])] = 1
			all_PIA.append((row['PIA_NPIA'] - row['Arrival']).total_seconds() / 60)

		if len(all_PIA) == 0: average_PIA = 0
		else: average_PIA = sum(all_PIA) / len(all_PIA)

		hour_vec = [0] * 24
		hour_vec[hour] = 1

		#past_hourly_data = weather_data['hourly']['data'][hour - 1]
		#last_hour_weather_vec = [past_hourly_data['summary'], past_hourly_data['temperature'], past_hourly_data['humidity'], past_hourly_data['windSpeed'], past_hourly_data['precipIntensity']]
		'''
		try:
			hourly_data = weather_data['hourly']['data'][hour]
			unique_hourly_summaries.append(hourly_data['summary'])
			current_hour_weather_vec = [hourly_data['summary'], hourly_data['temperature'], hourly_data['humidity'], hourly_data['windSpeed'], hourly_data['precipIntensity'], hourly_data['visibility'], hourly_data['dewPoint'], hourly_data['uvIndex']]

			PI_X_hourly.append(month_vec + weekday_vec + hour_vec + current_hour_weather_vec)
			PI_y_hourly.append(float(len(PI_selection)))

			PPH_X_hourly.append(doctor_vec + month_vec + weekday_vec + hour_vec + current_hour_weather_vec)
			PPH_y_hourly.append(float(len(PPH_selection)))

			PIA_y_hourly.append(average_PIA)
			PIA_X_hourly.append(doctor_vec + month_vec + weekday_vec + hour_vec + current_hour_weather_vec)

		except:
			continue
		'''

		PI_X_hourly.append(holiday_vec + month_vec + weekday_vec + hour_vec)
		PI_y_hourly.append(float(len(PI_selection)))

		PPH_X_hourly.append(doctor_vec + holiday_vec + month_vec + weekday_vec + hour_vec)
		PPH_y_hourly.append(float(len(PPH_selection)))

		PIA_X_hourly.append(doctor_vec + holiday_vec + month_vec + weekday_vec + hour_vec)
		PIA_y_hourly.append(average_PIA)



PPH_X_daily = np.array(PPH_X_daily)
PPH_y_daily = np.array(PPH_y_daily)

PI_X_daily = np.array(PI_X_daily)
PI_y_daily = np.array(PI_y_daily)

PIA_X_daily = np.array(PIA_X_daily)
PIA_y_daily = np.array(PIA_y_daily)

PPH_X_hourly = np.array(PPH_X_hourly)
PPH_y_hourly = np.array(PPH_y_hourly)

PI_X_hourly = np.array(PI_X_hourly)
PI_y_hourly = np.array(PI_y_hourly)

PIA_X_hourly = np.array(PIA_X_hourly)
PIA_y_hourly = np.array(PIA_y_hourly)


np.save('PPH_X_daily.npy', PPH_X_daily)
np.save('PPH_y_daily.npy', PPH_y_daily)
np.save('PI_X_daily.npy', PI_X_daily)
np.save('PI_y_daily.npy', PI_y_daily)
np.save('PIA_X_daily.npy', PIA_X_daily)
np.save('PIA_y_daily.npy', PIA_y_daily)

np.save('PPH_X_hourly.npy', PPH_X_hourly)
np.save('PPH_y_hourly.npy', PPH_y_hourly)
np.save('PI_X_hourly.npy', PI_X_hourly)
np.save('PI_y_hourly.npy', PI_y_hourly)
np.save('PIA_X_hourly.npy', PIA_X_hourly)
np.save('PIA_y_hourly.npy', PIA_y_hourly)


	# Add vectors to training data
	
#unique_hourly_summaries = sorted(list(set(unique_hourly_summaries)))
#unique_daily_summaries = sorted(list(set(unique_daily_summaries)))
'''
with open("PPH_X_daily.txt", "wb") as fp:
	pickle.dump(PPH_X_daily, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PPH_y_daily.txt", "wb") as fp:
	pickle.dump(PPH_y_daily, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PPH_X_hourly.txt", "wb") as fp:
	pickle.dump(PPH_X_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PPH_y_hourly.txt", "wb") as fp:
	pickle.dump(PPH_y_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PI_X_hourly.txt", "wb") as fp:
	pickle.dump(PI_X_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PI_y_hourly.txt", "wb") as fp:
	pickle.dump(PI_y_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)
'''

'''
with open("PIA_X_daily.txt", "wb") as fp:
	pickle.dump(PIA_X_daily, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PIA_y_daily.txt", "wb") as fp:
	pickle.dump(PIA_y_daily, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PIA_X_hourly.txt", "wb") as fp:
	pickle.dump(PIA_X_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("PIA_y_hourly.txt", "wb") as fp:
	pickle.dump(PIA_y_hourly, fp, protocol=pickle.HIGHEST_PROTOCOL)



with open("daily_summaries.txt", "wb") as fp:
	pickle.dump(unique_daily_summaries, fp, protocol=pickle.HIGHEST_PROTOCOL)

with open("hourly_summaries.txt", "wb") as fp:
	pickle.dump(unique_hourly_summaries, fp, protocol=pickle.HIGHEST_PROTOCOL)

'''