import pandas as pd
import numpy as np
import datetime
import os 

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

list_of_doctors = list(set(df_data['StaffMD'])) 

X = []
y = []
PI_dates = []

for index, row in df_data.iterrows():
	PI_dates.append(str(row['Arrival']).split(' ')[0])

df_data['PI_dates'] = PI_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

for date_code in unique_dates: 
    df_selection = df_data.ix[df_data["PI_dates"] == date_code]

    all_PIA = []
    doctor_vec = [0] * len(list_of_doctors)

    for index, row in df_selection.iterrows():
    	PIA_Time = (row['PIA_NPIA'] - row['Arrival']).total_seconds()
    	all_PIA.append(PIA_Time)

    	doctor_vec[list_of_doctors.index(row['StaffMD'])] = 1
    	current_weekday = row['Arrival'].weekday()

    average_PIA = sum(all_PIA) / len(all_PIA)

    weekday_vec = [0] * 7
    weekday_vec[current_weekday] = 1

    month_vec = [0] * 12
    month_int = int(date_code.split('-')[1])
    month_vec[month_int - 1] = 1

    X.append(doctor_vec + month_vec + weekday_vec)
    y.append(average_PIA / 60)



X = np.array(X)
y = np.array(y)

np.save('X_train_PIA_day.npy', X)
np.save('y_train_PIA_day.npy', y)