import pandas as pd
import numpy as np
import os 
import datetime

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()

list_of_doctors = list(set(df_data['StaffMD'])) 

X1 = []
X2 = []
X3 = []
y = []

date_codes = []

for index, row in df_data.iterrows():
    day = str(row['Arrival'].day)
    if len(day) == 1: day = '0' + str(row['Arrival'].day)
    current_date = str(row['Arrival'].month) + '-' + day + '-' + str(row['Arrival'].year)
    date_codes.append(current_date)

date_codes = np.array(date_codes)
df_data['date_codes'] = date_codes
date_codes_unique = np.sort(np.unique(date_codes))

for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]
    all_doctors = len(list(set(df_selection['StaffMD'])))
    PPHPD = len(df_selection) / all_doctors

    y.append(PPHPD)

    all_PIA = []

    for index, row in df_selection.iterrows():
    	PIA_Time = (row['PIA_NPIA'] - row['Arrival']).total_seconds()
    	all_PIA.append(PIA_Time)

    average_PIA = (sum(all_PIA) / len(all_PIA)) / 3600
    PI = len(df_selection)

    weekday_vec = [0] * 7
    current_weekday = datetime.datetime.strptime(date_code, "%m-%d-%Y").weekday()
    weekday_vec[current_weekday] = 1

    month_vec = [0] * 12
    month_int = int(date_code.split('-')[0])
    month_vec[month_int - 1] = 1

    X1.append(month_vec + weekday_vec)
    X2.append([PI] + month_vec + weekday_vec)
    X3.append([PI, average_PIA] + month_vec + weekday_vec)



X1 = np.array(X1)
X2 = np.array(X2)
X3 = np.array(X3)
y = np.array(y)

np.save('PPHPD_train_data_v1.npy', X1)
np.save('PPHPD_train_data_v2.npy', X2)
np.save('PPHPD_train_data_v3.npy', X3)
np.save('PPHPD_test_data.npy', y)