import pandas as pd
import numpy as np
import string
import os 
import datetime


# Read Dataset
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()


list_of_doctors = list(df_data["StaffMD"].unique())
print(len(list_of_doctors))

X = []      # training dataset  [hour and month]
y = []      # PPH time


date_codes = []
for index, row in df_data.iterrows():
    day = str(row["PIA_NPIA"].day)
    if len(str(row["PIA_NPIA"].day)) == 1:
        day = '0' + str(row["PIA_NPIA"].day)

    current_date = str(row["PIA_NPIA"].month) + '-' + day + '-' + str(row["PIA_NPIA"].year) + ' ' + str(int(row["PIA_NPIA"].hour))
    date_codes.append(current_date)

date_codes = np.array(date_codes)
df_data['date_codes'] = date_codes
date_codes_unique = np.sort(np.unique(date_codes))            #Getting unique date/hour to group rows per hour block

#for unique date codes
for index, date_code in enumerate(date_codes_unique): 
    print(str(index) + ' / ' + str(len(date_codes_unique)))
    df_selection = df_data.ix[df_data["date_codes"] == date_code]         #Grouping rows per unique date/hour block_hours					   

    #Doctor vector
    doctor_vector = [0] * len(list_of_doctors) 
    unique_doctors = list(set([row['StaffMD'] for index, row in df_selection.iterrows()]))

    for doc in unique_doctors:
        doc_index = list_of_doctors.index(doc)
        doctor_vector[doc_index] = 1

    #Define date
    only_date = date_code.split(' ')[0]
    month = int(only_date.split('-')[0])

    # initialize hour vector
    hour = int(date_code.split(' ')[1].strip())
    hour_vector = [0] * 24
    hour_vector[hour] = 1

    # initialize vectors for the month
    month_vec = [0] * 12
    month_vec[month - 1] = 1

    #Initialize vector for weekday
    weekday_vec = [0] * 7
    weekday = datetime.datetime.strptime(only_date, "%m-%d-%Y").weekday()
    weekday_vec[weekday] = 1

    # Add vectors to training data
    X.append(doctor_vector + hour_vector + month_vec + weekday_vec)
    tp = float(len(df_selection))                                      #Defining patients per hour (PPH) by taking the number of rows. 
    y.append(tp)


# convert lists to NumPy array
X = np.array(X)
y = np.array(y)

# save training and testing data as CSV files
np.savetxt("X_train_06-01-19.csv", X, delimiter=",")
np.savetxt("y_train_06-01-19.csv", y, delimiter=',')
