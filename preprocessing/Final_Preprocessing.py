#FINAL PREPROCESSING

import pandas as pd
import numpy as np
import os 
import datetime

CTAS_INVERSION = [5, 4, 3, 2, 1]

#READING EXCEL FILES
#df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = pd.read_excel("D:/EmergencyRoomEfficiency/dataset/Sample_data/test.xlsx")

#GET LIST OF INDIVIDUAL DOCTORS
list_of_doctors = sorted(list(set(df_data['StaffMD']))) 
#df_data.to_excel('2017_clean_data.xlsx')
#np.savetxt("list_of_doctors.csv", list_of_doctors, delimiter=';', fmt='%s')


df_data.drop(columns=['PtAccnt', 'HSC', 'Disposition', 'BedType', 'AreaofCare'], inplace=True)

X = []
y1 = []
y2 = []

for index, row in df_data.iterrows():
	print(index)
	ctas_vec = [0] * 5
	ctas_score = row['CTAS']
	ctas_vec[ctas_score - 1] = 1

	month_vec = [0] * 12
	weekday_vec = [0] * 7
	hour_vec = [0] * 24

	Arrival_Time = row['Arrival']
	Arrival_Hour = row['Arrival'].hour
	Month = row['Arrival'].month
	Weekday = Arrival_Time.weekday()

	month_vec[Month - 1] = 1
	hour_vec[Arrival_Hour] = 1
	weekday_vec[Weekday] = 1

	df_selection = df_data.drop(index).ix[(df_data['Arrival'] <= Arrival_Time) & (Arrival_Time <= df_data['DischargeTime'])]

	All_Patient_CTAS = []
	All_Doctors = []

	for i, row in df_selection.iterrows():
		All_Patient_CTAS.append(CTAS_INVERSION.index(row['CTAS']))
		All_Doctors.append(row['StaffMD'])

	#All_Patient_CTAS = [CTAS_INVERSION.index(row['CTAS']) for i, row in df_data.drop(index).iterrows() if row['Arrival'] <= Arrival_Time <= row['DischargeTime']]

	Number_Of_Patients = len(All_Patient_CTAS)
	ICTAS_score = sum(All_Patient_CTAS)

	All_Doctors = list(set(All_Doctors))
	doctor_vector = [0] * len(list_of_doctors)

	#All_Doctors = list(set([row['StaffMD'] for i, row in df_data.drop(index).iterrows() if row['Arrival'] <= Arrival_Time <= row['DischargeTime']]))

	for doctor in All_Doctors:
		doc_i = list_of_doctors.index(doctor)
		doctor_vector[doc_i] = 1

	LOS = (row['DischargeTime'] - row['Arrival']).total_seconds()
	PIA = (row['PIA_NPIA'] - row['Arrival']).total_seconds()

	training_vector = (ctas_vec + month_vec + weekday_vec + hour_vec + doctor_vector)		
	training_vector.extend([Number_Of_Patients, ICTAS_score, len(All_Doctors)])

	print(training_vector)

	X.append(training_vector)
	y2.append(LOS)
	y1.append(PIA)




'''
X = np.array(X)
y1 = np.array(y1)
y2 = np.array(y2)

np.save('IWAITTIME_train_data.npy', X)
np.save('IPIA_test.npy', y1)
np.save('ILOS_test.npy', y2)
'''