import datetime
import pandas as pd
import numpy as np
import os 
import matplotlib.pyplot as plt

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()

all_PPH_dates = []
all_PI_dates = []

for index, row in df_data.iterrows():
	all_PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0])
	all_PI_dates.append(str(row['Arrival']).split(' ')[0])

df_data['PPH_codes'] = all_PPH_dates
df_data['PI_codes'] = all_PI_dates


base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

PPH_list = []
PI_list = []

for date in unique_dates:
	PPH_selection = df_data.ix[df_data['PPH_codes'] == date]
	PI_selection = df_data.ix[df_data['PI_codes'] == date]

	PPH_list.append(len(PPH_selection))
	PI_list.append(len(PI_selection))



plt.plot(range(len(PPH_list)), PPH_list, label="PPH per day")
plt.title('Realtime patients seen per day in 2017')
plt.xlabel('Number of days')
plt.ylabel('Number of patients seen')
plt.savefig('PPH_2017_realtime.png', dpi=500)
plt.close()

plt.plot(range(len(PI_list)), PI_list, label="PI per day")
plt.title('Realtime patient inflow in 2017')
plt.xlabel('Number of days')
plt.ylabel('Number of patients entering ER')
plt.savefig('PI_2017_realtime.png', dpi=500)
plt.close()