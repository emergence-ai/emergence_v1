#Add data to numpy data and train with linear regression

import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import datetime

PI_X_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PI_Per_Day_Weather_Data/X_train_weather.npy")
PPH_X_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PPH_Per_Day_Weather_Data/X_train_weather.npy")
PIA_X_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PIA_Per_Day_Weather_Data/X_train_weather.npy")

PI_y_train = np.load("C:/Users/slu13/Desktop/Model/dataset/FINAL_DATA/WEATHER/PI_Per_Day_Weather_Data/y_train_weather.npy")

raptors = pd.read_excel("C:/Users/slu13/Desktop/raptors.xlsx")
bluejays = pd.read_excel("C:/Users/slu13/Desktop/bluejays.xlsx")
mapleleafs = pd.read_excel("C:/Users/slu13/Desktop/mapleleafs.xlsx")
events = pd.read_excel("C:/Users/slu13/Desktop/events.xlsx")


fullMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'Octrobre', 'November', 'December']
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def create_basic_event_array(dataframe, dateArray):
	eventArray = [0] * len(dateArray)
	for index, date in enumerate(unique_dates):
		day = int(date.split('-')[2])
		month = int(date.split('-')[1])
		for i, row in dataframe.iterrows():
			if "to" in row['date']:
				firstdate = row['date'].split('to')[0]
				seconddate = row['date'].split('to')[1]

				day1 = int(firstdate.split(' ')[2])
				month1 = fullMonths.index(firstdate.split(' ')[1]) + 1
				day2 = int(seconddate.split(' ')[2])
				month2 = fullMonths.index(seconddate.split(' ')[1]) + 1

				if day == day1 and month == month1:
					eventArray[index] += 1
				elif day == day2 and month == month2:
					eventArray[index] += 1

			elif "and" in row['date']:
				firstdate = row['date'].split('and')[0]
				seconddate = row['date'].split('and')[1]

				day1 = int(firstdate.split(' ')[2])
				month1 = fullMonths.index(firstdate.split(' ')[1]) + 1
				day2 = int(seconddate.split(' ')[2])
				month2 = fullMonths.index(seconddate.split(' ')[1]) + 1

				if day == day1 and month == month1:
					eventArray[index] += 1
				elif day == day2 and month == month2:
					eventArray[index] += 1
				
			else: 
				day1 = int(row['date'].split(' ')[2])
				month1 = fullMonths.index(row['date'].split(' ')[1]) + 1

				if day == day1 and month == month1:
					eventArray[index] += 1

	return eventArray


def create_raptors_array(dataframe, dateArray):
	eventArray = [0] * len(dateArray)
	for index, date in enumerate(unique_dates):
		day = int(date.split('-')[2])
		month = int(date.split('-')[1])
		for i, row in dataframe.iterrows():
			day1 = int(row['Date'].split(',')[1].split(' ')[2])
			month1 = months.index(row['Date'].split(',')[1].split(' ')[1].strip()) + 1
			if day == day1 and month == month1:
				if row['Playoffs'] == 1:
					eventArray[index] += 2
				else: eventArray[index] += 1
	return eventArray

def create_blueJays_array(dataframe, dateArray):
	eventArray = [0] * len(dateArray)
	for index, date in enumerate(unique_dates):
		day = int(date.split('-')[2])
		month = int(date.split('-')[1])
		for i, row in dataframe.iterrows():
			day1 = int(row['Date'].split(',')[1].split(' ')[2])
			month1 = months.index(row['Date'].split(',')[1].split(' ')[1].strip()) + 1
			if day == day1 and month == month1:
				eventArray[index] += row['Attendance'] / 53506
	return eventArray

def create_mapleLeafs_array(dataframe, dateArray):
	eventArray = [0] * len(dateArray)
	for index, date in enumerate(unique_dates):
		for i, row in dataframe.iterrows():
			if date.strip() == row['Date'].strip():
				eventArray[index] += row['Attendance'] / 19800

	return eventArray

print(PI_X_train.shape)
print(PPH_X_train.shape)
print(PIA_X_train.shape)


base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

raptorsArr = create_raptors_array(raptors, unique_dates)
bluejaysArr = create_blueJays_array(bluejays, unique_dates)
mapleleafsArr = create_mapleLeafs_array(mapleleafs, unique_dates)
eventArr = create_basic_event_array(events, unique_dates)

total_events = [0] * 365

for index, (raps, bj, ml, ev) in enumerate(zip(raptorsArr, bluejaysArr, mapleleafsArr, eventArr)):
	if raps != 0: total_events[index] += 1
	if bj != 0: total_events[index] += 1
	if ml != 0: total_events[index] += 1
	if ev != 0: total_events[index] += 1

zero_events = []
one_event = []
two_events = []
three_events = []


for index, PI in enumerate(list(PI_y_train)):
	if total_events[index] == 0: zero_events.append(PI)
	elif total_events[index] == 1: one_event.append(PI)
	elif total_events[index] == 2: two_events.append(PI)
	elif total_events[index] == 3: three_events.append(PI)

zero_events = sum(zero_events) / len(zero_events)
one_event = sum(one_event) / len(one_event)
two_events = sum(two_events) / len(two_events)
three_events = sum(three_events) / len(three_events)

x = range(0, 4)
numOfEvents = [0, 1, 2, 3]

plt.xticks(x, numOfEvents)
plt.bar(x, [zero_events, one_event, two_events, three_events], width=0.35, color='orange')
plt.xlabel("Nombre d'événements dans la ville", fontsize=9)
plt.ylabel('Nombre de patients moyen par jour', fontsize=9)
plt.title("Nombre moyen de patients par jour selon la quantité d'événements dans la ville", fontsize=9)
#plt.savefig('API_per_NOD', dpi=500)
#plt.close()
plt.show()



'''
PIA_X_train = np.insert(PIA_X_train, -1, raptorsArr, axis=1)
PIA_X_train = np.insert(PIA_X_train, -1, bluejaysArr, axis=1)
PIA_X_train = np.insert(PIA_X_train, -1, mapleleafsArr, axis=1)
PIA_X_train = np.insert(PIA_X_train, -1, eventArr, axis=1)

PPH_X_train = np.insert(PPH_X_train, -1, raptorsArr, axis=1)
PPH_X_train = np.insert(PPH_X_train, -1, bluejaysArr, axis=1)
PPH_X_train = np.insert(PPH_X_train, -1, mapleleafsArr, axis=1)
PPH_X_train = np.insert(PPH_X_train, -1, eventArr, axis=1)

print(PIA_X_train.shape)
print(PPH_X_train.shape)

np.save('X_train_events.npy', PIA_X_train)
np.save('X_train_events_pph.npy', PPH_X_train)
'''