import pandas as pd
import numpy as np
import string
import os 
import csv
import sys
import math
from io import StringIO
import pickle

import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import regularizers
from sklearn.metrics import mean_squared_error, log_loss

from sklearn.linear_model import LinearRegression, SGDClassifier
from sklearn.decomposition import PCA
from sklearn.model_selection import learning_curve, train_test_split
from sklearn.model_selection import ShuffleSplit
from sklearn.neighbors import KNeighborsRegressor
import statsmodels.api as sm

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from numpy import genfromtxt

'''
X = pd.read_csv('X_train_PI_day_10-01-19.csv', sep=',', header=None)
y = pd.read_csv('y_train_PI_day_10-01-19.csv', sep=',', header=None)

print(X.shape)
print(y.shape)
'''
'''
X = pd.read_csv('X_train_22-08-18.csv', sep=',', header=None)
y = pd.read_csv('y_train_22-08-18.csv', sep=',', header=None)
'''

X = np.load('X_train_events.npy')
y = np.load('y_train_events.npy')

print(X.shape, y.shape)


#y2 = np.load('ALOS_test.npy')

'''
X = np.load('X_train_weather.npy')
y = np.load('y_train_weather.npy')
'''
'''
X1 = np.load('PPHPD_train_data_v1.npy') 
X2 = np.load('PPHPD_train_data_v2.npy') 
X3 = np.load('PPHPD_train_data_v3.npy') 
y = np.load('PPHPD_test_data.npy') 
'''
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, shuffle=True)


# Build model

def binary_network(X, y):
    """
    Neural network used for binary classification of PPH threshold of 8
    :param: X: training data input
    :param: y: labels
    """
    model = Sequential()
    model.add(Dense(20, input_dim=X.shape[1], activation="relu", kernel_initializer="normal"))
    model.add(Dropout(0.8))
    model.add(Dense(10, activation="relu", kernel_initializer="normal"))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation="sigmoid", kernel_initializer="normal"))
    model.compile(loss="mean_squared_error", optimizer="Adam",metrics=['acc'])
    return model

def wide_regression_network(X, y):
    """
    Wide neural network used for regression with PPH
    :param: X: training data input
    :param: y: labels
    """
    model = Sequential()
    model.add(Dense(256, input_dim=X.shape[1], activation="relu"))
    model.add(Dropout(0.8))
    model.add(Dense(20, activation="relu"))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    model.compile(loss="mean_squared_error", optimizer="adam",metrics=['accuracy'])
    return model 

def plot_graphs(history):
    """
    Function used to plot accuracy and loss of model
    :param: history: from Sequential()
    """
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig("acc.png")
    plt.close()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig("loss.png")

    plt.show()

def complex_sequential(X, y):
    # Build model
    model = Sequential()
    model.add(Dense(256, input_dim=X.shape[1], activation="relu"))
    model.add(Dropout(0.8))
    model.add(Dense(128, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation="relu"))
    model.add(Dropout(0.3))
    model.add(Dense(20, activation="relu"))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    # adam = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss="mean_squared_error", optimizer="adam", metrics=['accuracy'])
    return model

'''
# build model
model = complex_sequential(X, y)

# Run model
#earlystopper = EarlyStopping(patience=100, verbose=1)
#checkpointer = ModelCheckpoint('ER-WaitingTimes.h5', verbose=1, save_best_only=True)
history = model.fit(X, y, epochs= 50, batch_size=5, validation_split=0.2)


plot_graphs(history)
'''
'''
neigh = KNeighborsRegressor(n_neighbors=10)
neigh.fit(X_train, y_train)
score = neigh.score(X_test, y_test)

print(score)

predictions = neigh.predict(X_test)
mse = mean_squared_error(y_test, predictions)

print(mse)
'''

clf = LinearRegression(n_jobs=-1)
clf.fit(X_train, y_train)
print(clf.score(X_test, y_test))

predictions = clf.predict(X)
mse = mean_squared_error(y, predictions)
#loss = log_loss(y, predictions)

print(mse)


model = sm.OLS(y, X).fit()
predictions = model.predict(X) # make the predictions by the model
mse = mean_squared_error(y, predictions)

print(mse)

# Print out the statistics
print(model.summary())


with open('PPH_per_day.pickle', 'wb') as handle:
    pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

'''
with open('PI_per_day.pickle', 'rb') as handle:
    model = pickle.load(handle)

predictions = model.predict(X)

mse = mean_squared_error(y, predictions)
print(mse)
'''
'''

pca = PCA(n_components=2) #2-dimensional PCA
X_transform = pca.fit_transform(X)

fig = plt.figure()
ax = plt.add_subplot(111, projection='3d')
for x, y1 in zip(X, y):
    x1 = x[0]
    x2 = x[1]

    ax.scatter(x1, x2, y1, c='blue')

plt.show()
'''