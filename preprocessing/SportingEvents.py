#Converting html sporting event data to excel format

import pandas as pd 

def get_2017_data(dataframe):
	for index, row in dataframe.iterrows():
		if row["GP"] == "GP":
			dataframe.drop(index, inplace=True)
			continue
		currentDate = row['Date']
		currentYear = int(currentDate.split('-')[0].strip())
		if currentYear != 2017: dataframe.drop(index, inplace=True)
	return dataframe

def to_datetime(dataframe):
	raw_dates = dataframe['Date'].tolist()
	pass

def add_playoffs(dataframe):
	playoffIndicator = [1] * len(dataframe)
	dataframe['Playoffs'] = playoffIndicator
	return dataframe

raptors_link = ["https://www.basketball-reference.com/teams/TOR/2017_games.html", "https://www.basketball-reference.com/teams/TOR/2018_games.html"]
blueJays_link = "https://www.baseball-reference.com/teams/TOR/2017-schedule-scores.shtml"
mapleLeafs_link = ["https://www.hockey-reference.com/teams/TOR/2017_games.html", "https://www.hockey-reference.com/teams/TOR/2018_games.html"]


'''
mapleLeafs_df = pd.read_html(io=mapleLeafs_link[0])
mapleLeafs_df2 = pd.read_html(io=mapleLeafs_link[1])
mapleLeafs_reg_season = get_2017_data(mapleLeafs_df[0])
mapleLeafs_reg_season2 = get_2017_data(mapleLeafs_df2[0])
mapleLeafs_playoffs = get_2017_data(mapleLeafs_df[1])

mapleLeafs_reg_season.drop(columns=['GP', 'Unnamed: 2', 'Opponent', 'GF', 'GA', 'Unnamed: 6', 'Unnamed: 7', 'W', 'L', 'OL', 'Streak', 'LOG', 'Notes'], inplace=True)
mapleLeafs_reg_season2.drop(columns=['GP', 'Unnamed: 2', 'Opponent', 'GF', 'GA', 'Unnamed: 6','Unnamed: 7', 'W', 'L', 'OL', 'Streak', 'LOG', 'Notes'], inplace=True)
mapleLeafs_playoffs.drop(columns=['GP', 'Unnamed: 2', 'Opponent', 'GF', 'GA', 'Unnamed: 6', 'Unnamed: 7', 'W', 'L', 'Streak', 'LOG', 'Notes'], inplace=True)

mapleLeafs_playoffs = add_playoffs(mapleLeafs_playoffs)

mapleLeafs_data = pd.concat([mapleLeafs_reg_season, mapleLeafs_playoffs, mapleLeafs_reg_season2], sort=True)
mapleLeafs_data.set_index('Date', inplace=True)
mapleLeafs_data.fillna(0, inplace=True)

print(mapleLeafs_data)

mapleLeafs_data.to_excel("mapleLeafs.xlsx")
'''
'''
blueJays_df = pd.read_html(io=blueJays_link)[0]
blueJays_df = get_2017_data(blueJays_df)
blueJays_df.set_index('Date', inplace=True)
blueJays_df.drop(columns=['Gm#', 'Unnamed: 2', 'Tm', 'Unnamed: 4', 'Opp', 'W/L', 'R', 'RA', 'Inn',
       'W-L', 'Rank', 'GB', 'Win', 'Loss', 'Save', 'Time', 'D/N',
       'Streak', 'Orig. Scheduled'], inplace=True)

blueJays_df.to_excel('bluejays.xlsx')
'''
