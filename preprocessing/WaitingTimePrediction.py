import pandas as pd
import numpy as np
import os 

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()


list_of_doctors = list(set(df_data['StaffMD'])) 


X = []
y1 = []
y2 = []


date_codes = []
hour_codes = []
for index, row in df_data.iterrows():
    day = str(row['Arrival'].day)
    if len(day) == 1: day = '0' + str(row['Arrival'].day)

    number_of_hours_per_block = 1
    current_date = str(row['Arrival'].month) + '-' + day + '-' + str(row['Arrival'].year) + ' ' + str(int(row['Arrival'].hour / number_of_hours_per_block))
    date_codes.append(current_date)
    hour_codes.append(int(row['Arrival'].hour / number_of_hours_per_block))

date_codes = np.array(date_codes)
hour_codes = np.array(hour_codes)

df_data['date_codes'] = date_codes
df_data['hour_codes'] = hour_codes

date_codes_unique = np.sort(np.unique(date_codes))

for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]

    all_PIA = []
    all_LOS = []
    doctor_vec = [0] * len(list_of_doctors)

    for index, row in df_selection.iterrows():
    	PIA_Time = (row['PIA_NPIA'] - row['Arrival']).total_seconds()
    	LOS_Time = (row['DischargeTime'] - row['Arrival']).total_seconds()
    	all_PIA.append(PIA_Time)
    	all_LOS.append(LOS_Time)

    	doctor_vec[list_of_doctors.index(row['StaffMD'])] = 1
    	current_weekday = row['PIA_NPIA'].weekday()

    average_PIA = sum(all_PIA) / len(all_PIA)
    average_LOS = sum(all_LOS) / len(all_LOS)

    hour = int(date_code.split(' ')[1])
    hour_vec = [0] * 24
    hour_vec[hour] = 1

    weekday_vec = [0] * 7
    weekday_vec[current_weekday] = 1

    month_vec = [0] * 12
    month_int = int(date_code.split('-')[0])
    month_vec[month_int - 1] = 1

    X.append(doctor_vec + hour_vec + month_vec + weekday_vec)
    y1.append(average_PIA / 60)
    y2.append(average_LOS / 60)


X = np.array(X)
y1 = np.array(y1)
y2 = np.array(y2)

np.save('X_train_PIA_hour.npy', X)
np.save('y_train_PIA_hour.npy', y1)
#np.save('ALOS_test.npy', y2)