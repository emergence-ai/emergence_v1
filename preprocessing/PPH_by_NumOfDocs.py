import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial.polynomial import polyfit

#READING EXCEL FILES
df_data = pd.read_excel('C:/Users/slu13/Desktop/2017_clean_data.xlsx', header=0).dropna()
df_data = df_data.sort_values(by='PIA_NPIA', ascending=True)

PPH_dates = []

for index, row in df_data.iterrows():
	PPH_dates.append(str(row['PIA_NPIA']).split(' ')[0])

df_data['PPH_dates'] = PPH_dates

base = datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
unique_dates = [str(base + datetime.timedelta(days=x)).split(' ')[0] for x in range(0, 365)]

PPH_data = []
doctor_data = []

for date_code in unique_dates:
	df_selection = df_data.ix[df_data['PPH_dates'] == date_code]
	unique_doctors = list(set(list(df_selection['StaffMD'])))
	number_of_doctors = len(unique_doctors)
	patients_seen = len(df_selection)
	PPH_data.append(patients_seen)
	doctor_data.append(number_of_doctors)

total_doctor = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23]
total_PPH = []
for x in total_doctor:
	current_PPH = []
	for number_docs, PPH in zip(doctor_data, PPH_data):
		if number_docs == x: current_PPH.append(PPH)

	total_PPH.append(current_PPH)

PPH_mean = [(sum(x) / len(x)) for x in total_PPH]

for xe, ye in zip(total_doctor, total_PPH):
	plt.scatter([xe] * len(ye), ye, c='#a6a6a6', s=20)
plt.scatter(total_doctor, PPH_mean, c='r', s=60, label='Average value')

plt.plot(np.unique(total_doctor), np.poly1d(np.polyfit(total_doctor, PPH_mean, 1))(np.unique(total_doctor)), c='k')
plt.title('Number of patients seen per day according to number of doctors on shift')
plt.xlabel('Number of doctors on shift')
plt.ylabel('Patients seen')
plt.savefig('Patients seen per day per number of docs', dpi=500)
plt.close()