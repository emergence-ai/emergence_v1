# Emergence_v1

-Temporary Version Of Emergence Adapted For Hospital Trial And Use

***WORK LEFT TO DO***

-Add graph functionality for PI, PPH and PIA per hour
-Add overcrowding functionality (Many red hours in a row)
-Add dashboard with stats and recommendations for each day schedule (analysis.html)
-Implement security
-Train data (10 years)
-Change schedule (in progress)
-Event importance

Extra: -Add functionality for user to test different doctors in recommended changes

# Documentation

## Variable names

* df_schedule: Pandas dataframe of schedules. Each row represents one day. Each column is a shift

* day_schedule: Pandas dataframe. Each row is an hour of a 24 hour clock from 7:00 AM to 6:00 AM the following day

* dateID: int from [0, number_of_days - 1] used to access PPH, PI and schedule values

* schedule_tables_dict: table of index: Pandas dataframe

* list_of_doctors_total_csv: list of all doctors